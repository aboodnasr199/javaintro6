package casting;

public class CastingCharacters {
    public static void main(String[] args) {

        int i1 = 65;

        char c1 = (char) i1;

        System.out.println(c1);

        char c2 = 97;
        System.out.println(c2);

        char char1 = 'J';
        char char2 = 'o';
        char char3 = 'h';
        char char4 = 'n';

        System.out.println(char1 + char2 + char3 + char4); // adding each letter from ascii table 399
        System.out.println("" + char1 + char2 + char3 + char4); // returns the characters of john when adding ""


    }
}
