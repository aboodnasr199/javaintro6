package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcher {
    public static void main(String[] args) {
       String input = ScannerHelper.getString();
        System.out.println(Pattern.matches("[a-z0-9_ -]{3,10}", input));
        //java -> true
        //

       Pattern pattern = Pattern.compile("Java");
       Matcher matcher = pattern.matcher("I love Java, Java is fun");

       System.out.println(pattern); // prints out compiled regex pattern as a pattern
       System.out.println(pattern.pattern()); // prints out compiled regex pattern as a Stting
        System.out.println(pattern.toString()); // prints out compiled regex pattern as a String

        System.out.println(matcher.matches());

        System.out.println();

        int counter = 0;
        while (matcher.find()){ // match.find - > finds the next matched word in the given string and returns t or false
            counter++;
            System.out.println(matcher.group()); // prints out the found matched word
            System.out.println(matcher.start()); // prints out the started index of the matched word
            System.out.println(matcher.end()); // prints out the ending index of the matched word
        }
        System.out.println("Java count = " + counter);
    }
}
