package collections;

import java.util.*;

public class _03_Map {
    public static void main(String[] args) {
        /*
        Map is an interface and it has some implementation classes
        1. HashMap: it is the most common implementation class
            - it stores data as keyvalue pairs
            -keyes are always unique - one null
            - keys can be considered as identifiers
            - values can be duplicated
            - no insertion order
        2. LinkedHashMap
        3. TreeMap
         */


        System.out.println("\n------HashMap-----\n");

        HashMap<String, Integer> hashMap = new HashMap<>();

        hashMap.put("Sandina", 25);
        hashMap.put("Zel", 20);
        hashMap.put("Assem", 27);
        hashMap.put("Assem", 20);
        hashMap.put("Okan", 18);
        hashMap.put(null, 18);
        hashMap.put(null, 100);
        hashMap.put("", null);
        hashMap.put("Anton", null);
        hashMap.put("Jazzy", null);


        System.out.println(hashMap);
        System.out.println(hashMap.size());




        System.out.println("\n------LinkedHashMap-----\n");


        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<>();

        linkedHashMap.put("Sandina", 25);
        linkedHashMap.put("Zel", 20);
        linkedHashMap.put("Assem", 27);
        linkedHashMap.put("Assem", 20);
        linkedHashMap.put("Okan", 18);
        linkedHashMap.put(null, 18);
        linkedHashMap.put(null, 100);
        linkedHashMap.put("", null);
        linkedHashMap.put("Anton", null);
        linkedHashMap.put("Jazzy", null);

        System.out.println(linkedHashMap);


        System.out.println("\n------TreeMap-----\n");

        TreeMap<String, Integer> treeMap = new TreeMap<>();

        treeMap.put("Sandina", 25);
        treeMap.put("Zel", 20);
        treeMap.put("Assem", 27);
        treeMap.put("Assem", 24);
        treeMap.put("Jazzy", null);
        treeMap.put("anton", null);


        System.out.println(treeMap);

        System.out.println(treeMap.firstKey());
        System.out.println(treeMap.lastKey());
        System.out.println(treeMap.firstEntry());
        System.out.println(treeMap.lastEntry());


        System.out.println("\n------HashTable-----\n");

        Hashtable<String, Integer> hashtable = new Hashtable<>();
        // HASHTable does not allow null key or values
        hashtable.put("Adam", 20);
        //hashtable.put(null, 25); // null pointer exception
        //hashtable.put("Yana", null); // null pointer exception

        System.out.println(hashtable);


        System.out.println("\n------------hashmap, linkedhashmap, hashtable, treemap, in the shape of a map----------\n");

        Map<Integer, Integer> number1 = new HashMap<>();
        Map<Integer, Integer> number2 = new LinkedHashMap<>();
        Map<Integer, Integer> number3 = new TreeMap<>();
        Map<Integer, Integer> number4 = new Hashtable<>();










    }
}
