package utilities;

import java.util.Random;

public class RandomGenerator {

    static Random r = new Random();

    public static int getRandomNumber(int start, int end){
        return r.nextInt(end - start + 1) + start;
    }

}
