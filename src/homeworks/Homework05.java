package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n------Task1-----\n");

        for (int i = 1; i <= 100 ; i++) {
            if(i % 7 == 0) System.out.println(i);
        }
        System.out.println("\n------Task2-----\n");

        for (int i = 1; i <= 50 ; i++) {
            if (i % 2 == 0 && i % 3 == 0) System.out.println(i);
        }
        System.out.println("\n------Task3-----\n");

        for (int i = 100; i >= 50; i--) {
            if (i % 5 == 0) System.out.println(i);
        }
        System.out.println("\n------Task4-----\n");

        for (int i = 0; i <= 7 ; i++) {
            System.out.println(i * i);
        }
        System.out.println("\n------Task5-----\n");

        int sum = 0;
        for (int i = 1; i <= 10 ; i++) {
            System.out.println(sum += i);
        }
        System.out.println("\n------Task6-----\n");

        int factor = ScannerHelper.getNumber();
        int factorial = 1;

        for (int i = 1; i <= factor ; i++) {
            System.out.println(factorial *= i);
        }
        System.out.println("\n------Task7-----\n");

        String str = ScannerHelper.getFirstName();

        int vowel = 0;
        for (int i = 0; i < str.length() ; i++) {
           char c =  Character.toLowerCase(str.charAt(i));
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') vowel++;
        }
        System.out.println("There are " + vowel + " vowels in this fullname");

        System.out.println("\n------Task8-----\n");
        String name = ScannerHelper.getFirstName();

        while (!name.startsWith("j") && !name.startsWith("J")){
            name = ScannerHelper.getFirstName();

        }
        System.out.println("End of program");
    }

}
