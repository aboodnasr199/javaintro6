package enums;

public class Practice01 {
    public static void main(String[] args) {

    DaysOfTheWeek dayByUser = DaysOfTheWeek.Sunday;

    switch (dayByUser){
        case Saturday:
        case Sunday:
            System.out.println("It is OFF today");
            break;
        case Monday:
        case Tuesday:
        case Wednesday:
        case Thursday:
        case Friday:
            System.out.println("It is work day");
        default:
            throw new RuntimeException("No Such enum value");
    }
    }
}
