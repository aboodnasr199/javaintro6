package arrays;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {
        // create an int array that will store 6 numbers
        int[] numbers = new int[6];
        System.out.println(Arrays.toString(numbers));

        //how to assign a value to existing index
        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;
        System.out.println(Arrays.toString(numbers));

        //print each element with for loop
        System.out.println("\n=====For loop=====\n");

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

        }
        System.out.println("\n== for loop - enhanced loop ===\n");
        for (int nums : numbers){
            System.out.println(nums);
        }

        //iter - shortcut for enhanced loop

    }
}
