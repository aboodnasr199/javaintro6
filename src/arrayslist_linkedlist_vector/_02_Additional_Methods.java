package arrayslist_linkedlist_vector;

import java.util.ArrayList;

public class _02_Additional_Methods {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");
        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(10);
        numbers.add(20);
        numbers.add(30);
        System.out.println(numbers);
        System.out.println(numbers.size());

        System.out.println("\n------contains() method------\n");
        // check if the list contains 5, 10 , 20
        System.out.println(numbers.contains(5));
        System.out.println(numbers.contains(10));
        System.out.println(numbers.contains(20));



        System.out.println("\n------index and last index of------\n");

        System.out.println(numbers.indexOf(10));
        System.out.println(numbers.indexOf(20));
        System.out.println(numbers.lastIndexOf(10));
        System.out.println(numbers.lastIndexOf(20));

        System.out.println(numbers.indexOf(15));
        System.out.println(numbers.lastIndexOf(15));

        System.out.println(numbers.indexOf(21)); // -1 = it does not exist
        System.out.println(numbers.lastIndexOf(21)); // -1

        System.out.println("\n------using remove method------\n");

        numbers.remove((Integer) 15);
        numbers.remove((Integer) 30);
        numbers.remove((Integer) 100); // nothing happens, does not remove it and returns false

        System.out.println(numbers);

        numbers.remove((Integer) 10); // removes the first occurance of 10

        System.out.println(numbers);

        // remove all elements that are 20

        numbers.removeIf(element -> element == 20); // lambda expression
        System.out.println(numbers);


        numbers.add(11);
        numbers.add(12);
        numbers.add(13);

        System.out.println(numbers);
        System.out.println(numbers.isEmpty());
        numbers.clear();

        System.out.println(numbers);
        System.out.println(numbers.isEmpty());
        System.out.println(numbers.size());



    }
}
