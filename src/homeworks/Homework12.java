package homeworks;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Homework12 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");
        System.out.println(noDigit("123Hello bjhbj"));
        System.out.println(noDigit("Java"));

        System.out.println("\n------Task2------\n");
        System.out.println(noVowel("xyz"));
        System.out.println(noVowel("JAVA"));

        System.out.println("\n------Task3------\n");
        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("Java"));
        System.out.println(sumOfDigits("John's age is 29"));

        System.out.println("\n------Task4------\n");
        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John's age is 29"));

        System.out.println("\n------Task5------\n");
        System.out.println(middleInt(1, 1, 1));
        System.out.println(middleInt(1, 2, 2));
        System.out.println(middleInt(5, 5, 8));
        System.out.println(middleInt(-1, 25, 10));

        System.out.println("\n------Task6------\n");
        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13, 13, 13})));

        System.out.println("\n------Task7------\n");
        System.out.println((Arrays.toString(arrFactorial(new int[]{1, 2, 3, 4}))));
        System.out.println((Arrays.toString(arrFactorial(new int[]{0, 5}))));
        System.out.println(Arrays.toString(arrFactorial(new int[]{5, 0, 6})));
        System.out.println(Arrays.toString(arrFactorial(new int[]{})));

        System.out.println("\n------Task8------\n");
        System.out.println(Arrays.toString(categorizeCharacters(new String("   "))));
        System.out.println(Arrays.toString(categorizeCharacters(new String("abc123$#%"))));
        System.out.println(Arrays.toString(categorizeCharacters(new String("12ab$%3c%"))));


    }

    public static String noDigit(String str) {

        return str.replaceAll("[0-9]", "");
    }

    public static String noVowel(String str) {
        return str.replaceAll("[aeiouAEIOU]", "");
    }

    public static int sumOfDigits(String str) {
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);

                count += digit;
            }

        }
        return count;
    }

    public static boolean hasUpperCase(String str) {

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (Character.isUpperCase(c)) {
                return true;
            }

        }
        return false;
    }

    public static int middleInt(int a, int b, int c) {

        int[] list = {a, b, c};
        Arrays.sort(list);

        return list[1];


    }

    public static int[] no13(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 13) {
                arr[i] = 0;
            }
        }
        return arr;

    }

    public static int[] arrFactorial(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int factorial = 1;
            for (int j = 1; j <= arr[i]; j++) {
                factorial *= j;
            }
            arr[i] = factorial;
        }
        return arr;
    }

    public static String[] categorizeCharacters(String str) {

        String[] newList = new String[3];

        String letters = str.replaceAll("[^a-zA-Z]", "");

        String digits = str.replaceAll("[\\D]", "");

        String special = str.replaceAll("[a-zA-Z0-9]", "");


        newList[0] = letters;
        newList[1] = digits;
        newList[2] = special;

        return newList;
    }
}
