package random_class;


import java.util.Random;

public class RandomNumbers {
    public static void main(String[] args) {

        /*
        get 2 random numbers bt 10 and 11

        0. Create a random class object from java.util
        1. find how many numbers do you have in your range
        2. put that in your nextInt() method
        3. add your smallest range number
         */
        Random r = new Random();

       int num1  = r.nextInt(2) + 10; // numbers bt 10-11 both inclusive
       int num2 = r.nextInt(2) + 10; // numbers bt 10-11 both inclusive

        System.out.println(num1);
        System.out.println(num2);

    }
}
