package operators.shorthand_operators;

public class ShortHandPractice {
    public static void main(String[] args) {

        int age = 45; // in 2023

        System.out.println("Age in 2023 = " + age);

        // What will be the age in 2028
        //age = age + 5;
        age += 5; // += known as shorthand operation which increases age by 5

        System.out.println(age);

        age -= 20;
        System.out.println(age);

        age /= 3;
        System.out.println(age);

        age %= 4;
        System.out.println(age);

    }
}
