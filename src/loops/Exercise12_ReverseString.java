package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();

        String reversedName = "";

        for (int i = str.length()-1; i >= 0; i--) {
            reversedName += str.charAt(i);
        }
        System.out.println(reversedName);

    }
}
