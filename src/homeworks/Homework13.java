package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("\n---------Task1-------\n");
        System.out.println(hasLowerCase(""));
        System.out.println(hasLowerCase("JAVA"));
        System.out.println(hasLowerCase("hello"));

        System.out.println("\n---------Task2-------\n");
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1, 1, 10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 1, 10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 0, 10))));

        System.out.println("\n---------Task3-------\n");
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1, 2, 3})));
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{0, 3, 6})));

        System.out.println("\n---------Task4-------\n");
        System.out.println(containsValue(new String[]{"abc", "foo", "java"}, "hello"));
        System.out.println(containsValue(new String[]{"abc", "def", "123"}, "ABC"));

        System.out.println("\n---------Task5-------\n");
        System.out.println(reverseSentence("Hello"));
        System.out.println(reverseSentence("Java is fun"));

        System.out.println("\n---------Task6-------\n");
        System.out.println(removeStringSpecialsDigits("123Java #$%is fun"));
        System.out.println(removeStringSpecialsDigits("Selenium"));

        System.out.println("\n---------Task7-------\n");
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123Java", "#$%is", "fun"})));

        System.out.println("\n---------Task8-------\n");
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")), new ArrayList<>(Arrays.asList("Java", "C#", "Python"))));

        System.out.println("\n---------Task9-------\n");
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("abc", "123", "#$%"))));


    }

    public static boolean hasLowerCase(String str) {


        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isLowerCase(c)) {
                return true;
            }

        }
        return false;
    }

    public static ArrayList<Integer> noZero(ArrayList<Integer> list) {

        ArrayList<Integer> newList = new ArrayList<>();
        for (Integer num : list) {
            if (num != 0) newList.add(num);
        }
        return newList;
    }
    public static int[][] numberAndSquare(int[] arr){
        int[][] result = new int[arr.length][2];

        for (int i = 0; i < arr.length; i++) {
            result[i][0] = arr[i];
            result[i][1] = arr[i] * arr[i];

        }
        return result;
    }
    public static boolean containsValue(String[] arr, String value) {
        for (String element : arr) {
            if (element.equals(value)) {
                return true;
            }
        }
        return false;
    }
    public static String reverseSentence(String sentence) {
        String[] words = sentence.split(" ");

        if (words.length < 2) {
            return "There is not enough words!";
        }

        StringBuilder reversedSentence = new StringBuilder();

        for (int i = words.length - 1; i >= 0; i--) {
            String word = words[i];
            reversedSentence.append((word)).append(" ");
        }

        return reversedSentence.toString().trim();
    }
    public static String removeStringSpecialsDigits(String input) {
        StringBuilder result = new StringBuilder();

        for (char c : input.toCharArray()) {
            if (Character.isLetter(c) || c == ' ') {
                result.append(c);
            }
        }

        return result.toString();
    }
    public static String[] removeArraySpecialsDigits(String[] array) {
        String[] result = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            StringBuilder filteredString = new StringBuilder();

            for (char c : array[i].toCharArray()) {
                if (Character.isLetter(c) || c == ' ') {
                    filteredString.append(c);
                }
            }

            result[i] = filteredString.toString();
        }

        return result;
    }
    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2) {
        HashSet<String> set1 = new HashSet<>(list1);
        HashSet<String> set2 = new HashSet<>(list2);

        set1.retainAll(set2);

        return new ArrayList<>(set1);
    }



    public static ArrayList<String> noXInVariables(ArrayList<String> list) {
        ArrayList<String> result = new ArrayList<>();

        for (String element : list) {
            if (!element.equalsIgnoreCase("x") && !element.equalsIgnoreCase("x") && !element.contains("x")) {
                result.add(element);
            }
        }

        return result;
    }



}
