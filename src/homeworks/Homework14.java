package homeworks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework14 {
        public static void main(String[] args) {

            System.out.println("\n------Task1------\n");
            fizzBuzz1(3);
            fizzBuzz1(18);
            fizzBuzz1(5);

            System.out.println("\n------Task2------\n");
            System.out.println(fizzBuzz2(0));
            System.out.println(fizzBuzz2(1));
            System.out.println(fizzBuzz2(3));

            System.out.println("\n------Task3------\n");
            System.out.println(findSumNumbers("abc$"));
            System.out.println(findSumNumbers("a1b4c  6#"));
            System.out.println(findSumNumbers("525"));

            System.out.println("\n------Task4------\n");
            System.out.println(findBiggestNumber("abc$"));
            System.out.println(findBiggestNumber("a1b4c  6#"));

            System.out.println("\n------Task5------\n");
            System.out.println(countSequenceOfCharacters(""));
            System.out.println(countSequenceOfCharacters("abc"));
            System.out.println(countSequenceOfCharacters("abbcca"));
            System.out.println(countSequenceOfCharacters("aaAAaa"));





        }

        public static void fizzBuzz1(int num) {
            for (int i = 1; i <= num; i++) {
                if (i % 3 == 0 && i % 5 == 0) {
                    System.out.println("FizzBuzz");
                } else if (i % 3 == 0) {
                    System.out.println("Fizz");
                } else if (i % 5 == 0) {
                    System.out.println("Buzz");
                } else {
                    System.out.println(i);
                }
            }
        }

        public static String fizzBuzz2(int num){
            if (num % 3 == 0 && num % 5 == 0){
                return "FizzBuzz";
            }else if(num % 3 == 0){
                return "Fizz";
            }else if(num % 5 == 0){
                return "Buzz";
            }else
                return String.valueOf(num);
        }

    public static int findSumNumbers(String input) {
        int sum = 0;
        StringBuilder number = new StringBuilder();

        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                number.append(c);
            } else {
                if (number.length() > 0) {
                    sum += Integer.parseInt(number.toString());
                    number.setLength(0);
                }
            }
        }

        if (number.length() > 0) {
            sum += Integer.parseInt(number.toString());
        }

        return sum;
    }

    public static int findBiggestNumber(String input) {
        int biggestNumber = 0;
        StringBuilder currentNumber = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (Character.isDigit(c)) {
                currentNumber.append(c);
            } else if (currentNumber.length() > 0) {
                int number = Integer.parseInt(currentNumber.toString());
                biggestNumber = Math.max(biggestNumber, number);
                currentNumber.setLength(0);
            }
        }

        if (currentNumber.length() > 0) {
            int number = Integer.parseInt(currentNumber.toString());
            biggestNumber = Math.max(biggestNumber, number);
        }

        return biggestNumber;
    }

    public static String countSequenceOfCharacters(String str) {
        if (str.isEmpty()) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        Pattern pattern = Pattern.compile("(.)\\1*");
        Matcher matcher = pattern.matcher(str);

        while (matcher.find()) {
            String sequence = matcher.group();
            result.append(sequence.charAt(0)).append(sequence.length());
        }

        return result.toString();
    }


}

