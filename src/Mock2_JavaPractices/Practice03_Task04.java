package Mock2_JavaPractices;

public class Practice03_Task04 {
    public static void main(String[] args) {
        String s = "xyzabc";
        firstLast(s);
    }
    public static void firstLast(String str){
        if (str.length() < 2){
            System.out.println("Length is less than 2");
        }
        else System.out.println(str.substring(0, 2).equals(str.substring(str.length() - 2)));
    }
}
