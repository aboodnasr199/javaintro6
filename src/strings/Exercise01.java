package strings;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        String is a reference type (object) that is used to store a sequence of characters.
        TEXTS
         */

        String name = "John"; //John
        String address = "Chicago IL 12345"; // Chicago IL 12345

        System.out.println(name);
        System.out.println(address);

        String favMovie = "Lord of the rings";
        System.out.println("My favorite movie is = " + favMovie);



    }
}
