package homeworks;

import java.util.HashMap;
import java.util.Map;

public class Homework19 {
    public static void main(String[] args) {
        System.out.println("\n---------Task1");
        System.out.println(sum(new int[]{1, 5, 10}, true));
        System.out.println(sum(new int[]{3, 7, 2, 5, 10}, false));

        System.out.println("\n---------Task2-------\n");

        System.out.println(nthChars("Java", 2));

        System.out.println("\n---------Task3--------\n");
        System.out.println(canFormString("Hello", "Hi"));

        System.out.println("\n---------Task4---------\n");
        System.out.println(isAnagram("Apple", "Peach"));


    }

    public static int sum(int[] array, boolean isEvenIndex) {
        int sum = 0;
        int startIndex = isEvenIndex ? 0 : 1;

        for (int i = startIndex; i < array.length; i += 2) {
            sum += array[i];
        }

        return sum;
    }

    public static String nthChars(String input, int n) {
        if (input.length() < n) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        for (int i = n - 1; i < input.length(); i += n) {
            result.append(input.charAt(i));
        }

        return result.toString();
    }

    public static boolean canFormString(String str1, String str2) {
        String s1 = str1.replaceAll("\\s", "").toLowerCase();
        String s2 = str2.replaceAll("\\s", "").toLowerCase();

        if (s1.length() != s2.length()) {
            return false;
        }

        Map<Character, Integer> charCount = new HashMap<>();
        for (char c : s1.toCharArray()) {
            charCount.put(c, charCount.getOrDefault(c, 0) + 1);
        }

        for (char c : s2.toCharArray()) {
            if (!charCount.containsKey(c)) {
                return false;
            }
            int count = charCount.get(c);
            if (count == 0) {
                return false;
            }
            charCount.put(c, count - 1);
        }

        return true;
    }

    public static boolean isAnagram(String str1, String str2) {
        String s1 = str1.replaceAll("\\s", "").toLowerCase();
        String s2 = str2.replaceAll("\\s", "").toLowerCase();

        if (s1.length() != s2.length()) {
            return false;
        }

        Map<Character, Integer> charCount = new HashMap<>();
        for (char c : s1.toCharArray()) {
            charCount.put(c, charCount.getOrDefault(c, 0) + 1);
        }

        for (char c : s2.toCharArray()) {
            if (!charCount.containsKey(c)) {
                return false;
            }
            int count = charCount.get(c);
            if (count == 0) {
                return false;
            }
            charCount.put(c, count - 1);
        }

        return true;
    }
}
