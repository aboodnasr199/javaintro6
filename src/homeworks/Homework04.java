package homeworks;

import utilities.ScannerHelper;

public class Homework04 {
    public static void main(String[] args) {
        String str = ScannerHelper.getFirstName();

        System.out.println("\n-------task1------\n");

        System.out.println("The length of the name is = " + str.length());

        System.out.println("The first character in the name is = " + str.charAt(0));

        System.out.println("The last character in the name is = " + str.charAt(str.length() - 1));

        System.out.println("The first 3 characters in the name is = " + str.substring(0, 3));

        System.out.println("The last 3 characters in the name is = " + str.substring(str.length() - 3));

        if (str.startsWith("A") || str.startsWith("a")){
            System.out.println("You are in the club");
        }
        else {
            System.out.println("Sorry you are not in the club");
        }



        System.out.println("\n-------task2------\n");

        String str1 = ScannerHelper.getAddress();

        String str2 = "3535 Abc St, Chicago, IL 12345";

        String str3 = "2860 S River Rd, Des Plaines, IL 60018";

        if (str1.equalsIgnoreCase(str2)){
            System.out.println("You are in the club");
        }
        else if (str1.equalsIgnoreCase(str3)){
            System.out.println("You are welcome to join the club");
        }
        else {
            System.out.println("Sorry, you will never be in the club");
        }



        System.out.println("\n-------task3------\n");

        String str4 = ScannerHelper.getFavCountry();

        if (str4.toLowerCase().contains("a") && str4.toUpperCase().contains("I")) {
            System.out.println("A and i are there");
        } else if (str4.toLowerCase().contains("a") && !str4.toUpperCase().contains("I")) {
            System.out.println("A is there");
        } else if (!str4.toLowerCase().contains("a") && str4.toUpperCase().contains("I")) {
            System.out.println("I is there");
        }
        else{
            System.out.println("A and i are not there");
        }

        System.out.println("\n-------task4------\n");

        String str5 = " java is Fun ";

        System.out.println(str5);

        System.out.println("The first word in the str is = " + str5.substring(0,5).trim());

        System.out.println("The second word in the str is = " + str5.substring(6,8).trim());

        System.out.println("The third word in the str is = " + str5.substring(9,12).trim());



    }
}