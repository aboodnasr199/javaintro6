package collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class _02_Set {
    public static void main(String[] args) {

        /*
        Set is an interface, and it has some class implementation as below
            -Set does not keep insertion order
            -Set does not allow duplicates
            -Set allow only 1 null element

            1. Hashset: The most common set implementation
                - it does not keep insertion order
                - it does not allow duplicates
                - it allows only 1 null element

           2. LinkedHashSet
                -it does keep insertion order
                -it does not allow duplicates
                -it allows only 1 null element

           3. TreeSet
                -it does keep insertion order
                -it does not allow duplicates
                -it DOES NOT ALLOW NULL element as it does not know how to sort it with numbers and strings
         */


        HashSet<String> objects = new HashSet<>();
        objects.add(null);
        objects.add("Sandina");
        objects.add(null);
        objects.add("Okan");
        objects.add("Alex");
        objects.add("Alex");
        objects.add("John");
        objects.add("abc");
        objects.add("123");
        objects.add("");
        objects.add("Sal");
        objects.add("Boo");

        System.out.println(objects); // null only allowed once even if you add multiple
        ArrayList<String> list = new ArrayList<>(objects); // in order to get the index of elements u must convert to an arraylist

        System.out.println(list.get(1)); // sandina

        objects.remove(null); // once null is removed they are all gone

        System.out.println(objects);


        System.out.println("\n--------LinkedHashSet------\n");

        LinkedHashSet<String> words = new LinkedHashSet<>();
        words.add(null);
        words.add("Sandina");
        words.add(null);
        words.add("Okan");
        words.add("Alex");
        words.add("Alex");
        words.add("John");
        words.add("abc");
        words.add("123");
        words.add("");
        words.add("Sal");
        words.add("Boo");

        System.out.println(words);


        System.out.println("\n--------TreeSet------\n");

        TreeSet<String> treeSet = new TreeSet<>();

        treeSet.add("Sandina");
        treeSet.add("Okan");
        treeSet.add("Alex");
        treeSet.add("Alex");
        treeSet.add("John");
        treeSet.add("abc");
        treeSet.add("123");
        treeSet.add("");
        treeSet.add("Sal");
        treeSet.add("Boo");

        System.out.println(treeSet);

        System.out.println(treeSet.descendingSet());

        System.out.println(treeSet.subSet("Boo", "Sandina"));

        System.out.println(treeSet.tailSet("Okan"));
        System.out.println(treeSet.headSet("Okan"));

        System.out.println(treeSet.lower("Okan")); // john
        System.out.println(treeSet.higher("Okan")); // sal


        System.out.println(treeSet.first()); // min number
        System.out.println(treeSet.last()); // max number



    }
}
