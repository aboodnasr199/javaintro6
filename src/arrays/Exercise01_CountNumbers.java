package arrays;

import java.util.Arrays;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {

        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};
        int number = 0;

        //write a program that counts how many negative numbers we have

        for (int j : numbers) {
            if (j < 0)
                number++;
        }
        System.out.println(number);

        // even numbers
        int evens = 0;

        for (int i : numbers) {
            if (i % 2 == 0) evens++;
        }
        System.out.println(evens);

        // write a program that finds the sum of all the numbers in the array
        int sum = 0;

        for (int k : numbers) {
            sum += k;
        }
        System.out.println(sum);
    }
}
