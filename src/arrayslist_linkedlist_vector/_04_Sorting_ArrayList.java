package arrayslist_linkedlist_vector;

import java.util.*;

public class _04_Sorting_ArrayList {
    public static void main(String[] args) throws InterruptedException {

        // Create an array list - linked list and Vector
        // arraylist syntax
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 15, 2, 5, -7));
        //LinkedList syntax
        LinkedList<String> colors = new LinkedList<>(Arrays.asList("Red", "Yellow", "Brown"));
        //Vector linkedList
        Vector<Double> balances = new Vector<>(Arrays.asList(10.3, 5.7, 10.0, 15.6));

        System.out.println("\n-------Print collections before sort");
        System.out.println(numbers);
        System.out.println(colors);
        System.out.println(balances);

        System.out.println("\n-------Print collections after sort");

        Collections.sort(numbers);
        Collections.sort(colors);
        Collections.sort(balances);

        System.out.println(numbers);
        System.out.println(colors);
        System.out.println(balances);

        System.out.println("\n-----Looping doubles-----\n");

        for (Double balance : balances) {
            System.out.println(balance);
            Thread.sleep(1000);
        }

    }
}
