package character_class;

import utilities.ScannerHelper;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {
        /*
        char is primitive data type
        character is a wrapper class
        character is object representation of char primitive

        Wrapper classes provides us some methods that allows to manipulate the data whereas primitive can't
         */
        String str = ScannerHelper.getString();
        // Print true if str starts with uppercase, else print false otherwise
        System.out.println(Character.isUpperCase(str.charAt(0)));
        System.out.println(Character.isLowerCase(str.charAt(0)));
        System.out.println(Character.isLetter(str.charAt(0)));
        System.out.println(Character.isLetterOrDigit(str.charAt(0)));
        System.out.println(Character.isDigit(str.charAt(0)));
        System.out.println(Character.isWhitespace(str.charAt(0))); // white space and spacechar is same
        System.out.println(Character.isSpaceChar(str.charAt(0))); // white space and spacechar is same
    }
}
