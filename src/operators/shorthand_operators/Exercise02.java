package operators.shorthand_operators;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {

        Scanner data = new Scanner(System.in);

        System.out.println("Hey user please enter your balance: ");
        double balance = data.nextDouble();

        System.out.println("The initial balance = $" + balance);

        System.out.println("What is the 1st transaction amount?");
        System.out.println("Balance after t1 =  $ " + (balance -= data.nextDouble()));

        System.out.println("What is 2nd transaction?");
        System.out.println("The balance after t2 = $" + (balance -= data.nextDouble()));

        System.out.println("What is the 3rd transaction?");
        System.out.println("The balance after t3 = $" + (balance -= data.nextDouble()));


    }
}
