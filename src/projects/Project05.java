package projects;


import math_class.Random;
import utilities.ScannerHelper;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n-------Task1------\n");

        String str = ScannerHelper.getString();
        int wordCount = 1;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isWhitespace(str.charAt(i))) wordCount++;

        }

        if (wordCount >= 2) System.out.println("This sentence has " + wordCount + " words.");
        else System.out.println("This sentence does not have multiple words");


        System.out.println("\n-------task2-------\n");
        Random random = new Random();

        int num1 = (int) (Math.random() * 26);
        int num2 = (int) (Math.random() * 26);


        System.out.println("Random number 1: " + num1);
        System.out.println("Random number 2: " + num2);

        int start = Math.min(num1, num2);
        int end = Math.max(num1, num2);

        for (int i = start; i <= end; i++) {
            if (i % 5 != 0) {
                System.out.print(i + " ");
            }
            System.out.println();
        }


        System.out.println("\n-------task3-------\n");

        String sentence = ScannerHelper.getString();
        int count = 0;

        for (int i = 0; i < sentence.length(); i++) {
            if (sentence.toLowerCase().charAt(i) == 'a') count++;


        }
        if (count == 0) System.out.println("This sentence does not have any characters");
        else System.out.println("This sentence has " + count + " a or A letters");


        System.out.println("\n-------task4-------\n");

        String word = ScannerHelper.getString();
        if (word.length() < 1) {
            System.out.println("This word does not have 1 or more characters in java");
            return;
        }

        boolean Palindrome = true;
        String back = "";
        for (int i = word.length() - 1; i >= 0; i--) {
            back += word.charAt(i);
            Palindrome = false;
            break;

        }

        if (Palindrome) {
            System.out.println("The word '" + word + "' is a palindrome.");
        } else {
            System.out.println("The word '" + word + "' is not a palindrome.");
        }


        System.out.println("\n------task5------\n");
        for (int i = 1; i <= 9; i++) {
            for (int s = 1; s <= 9 - i; s++) {
                System.out.print("   ");
            }
            for (int j = 1; j <= i * 2 - 1; j++) {
                System.out.print(" * ");
            }
            System.out.println();
        }
    }

}





