package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _09_Remove_Elements_removeif {
    public static void main(String[] args) {

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Red", "Purple", "Blue", "Yellow"));

        System.out.println("Before remove = " + colors);

        //remove the elements that are asked to be removed by using remove if == r

        colors.removeIf(element -> element.toLowerCase().contains("r"));
    }
}
