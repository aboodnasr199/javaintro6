package Mock2_JavaPractices;

public class Practice05_Task04 {
    public static void main(String[] args) {
        String[] list = {"banana", "orange", "Apple"};
        System.out.println(hasApple(list));

        String[] list2 = {"banana", "orange", "pineapple"};
        System.out.println(hasApple(list2));

    }
    public static boolean hasApple(String[] str){
        for (String element : str) {
            if (element.equalsIgnoreCase("apple"))
                return true;


        }
        return false;
    }
}
