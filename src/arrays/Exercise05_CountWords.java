package arrays;

import utilities.ScannerHelper;

import java.util.Arrays;

public class Exercise05_CountWords {
    public static void main(String[] args) {
        /*
        write a program that ask user to enter a string
        and count how many words are in the given string
         */
        String str = ScannerHelper.getString();

        int wordCount = 1;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isWhitespace(str.charAt(i))) wordCount++;
        }
        if (wordCount >= 1) System.out.println("This sentence has " + wordCount + " words.");


        System.out.println("\n------CHAR ARRAY VERSION-----\n");
        str = ScannerHelper.getString();
        int countSpace = 0;

        for (char c : str.toCharArray()) {
            if (Character.isWhitespace(c)) countSpace++;

        }
        System.out.println(countSpace + 1);


        System.out.println("\n-------2nd way------\n");

        String str2 = ScannerHelper.getString();

        System.out.println(str2.split(" ").length);

        System.out.println("\n----How to avoid miscount when there is more spaces----\n");

        String str3 = "Hello   World";

        String[] arr = str3.split(" ");

        System.out.println(Arrays.toString(arr));

        int actualWords = 0;
        for (String s : arr) {
            if (!s.isEmpty()) actualWords++;

        }
        System.out.println(actualWords);

    }
}



