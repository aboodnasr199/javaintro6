package loops.control_statements;

import utilities.ScannerHelper;

public class Exercise01_Break {
    public static void main(String[] args) {

        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        for (int i = Math.max(num1, num2); i >= Math.min(num1,num2); i--) {
            if (i < 10)break;
            System.out.println(i);
        }
    }
}
