package casting;

public class Excerise01 {
    public static void main(String[] args) {

        int num1 = 5, num2 = 2;

        System.out.println((double) num1 / num2); // returns 2.5

        System.out.println((double) num1 / num2);
        // or
        System.out.println(num1 / (double) num2);
        // or
        System.out.println((double) num1 / (double) num2);

    }
}
