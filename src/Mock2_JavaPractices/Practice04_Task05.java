package Mock2_JavaPractices;

import utilities.ScannerHelper;
import java.util.Arrays;

public class Practice04_Task05 {
    public static void main(String[] args) {
       int num = ScannerHelper.getNumber();
        System.out.println(Arrays.toString(fib(num)));
         int[] fib = new int[7];
         int first = 0;
         int second = 1;

        for (int i = 2; i < fib.length ; i++) {
            int nextNum = first + second;
            first = second;
            second = nextNum;
            fib[i] = nextNum;

        }
        System.out.println(Arrays.toString(fib));
    }
    public static int[] fib(int arr){
        int[] fibo = new int[arr];
        int first = 0;
        int second = 1;
        fibo[0] = first;
        fibo[1] = second;
        for (int i = 2; i < arr; i++) {
           int secondNum = first + second;
            first = second;
            second = secondNum;
            fibo[i] = secondNum;

        }
return fibo;
    }
    }


