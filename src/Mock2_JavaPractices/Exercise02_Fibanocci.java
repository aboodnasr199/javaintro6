package Mock2_JavaPractices;

import utilities.ScannerHelper;

import java.util.ArrayList;

public class Exercise02_Fibanocci {
    public static void main(String[] args) {
        //Fibanocci sequence
        //rule 1 = it always starts with 0 and 1

        ArrayList<Integer> answer = new ArrayList<>();
        int fib = ScannerHelper.getNumber();

        int first = 0;
        int second = 1;

        for (int i = 0; i < fib; i++) {
            answer.add(first);
            int nextNum = first + second;
            first = second;
            second = nextNum;

        }
        System.out.println(answer);
    }
}
