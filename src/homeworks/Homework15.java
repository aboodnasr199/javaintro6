package homeworks;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Homework15 {
    public static void main(String[] args) {
        System.out.println("\n------Task1------\n");
        System.out.println(Arrays.toString(fibonacciSeries1(3)));
        System.out.println(Arrays.toString(fibonacciSeries1(5)));
        System.out.println(Arrays.toString(fibonacciSeries1(7)));


        System.out.println("\n------Task2------\n");
        System.out.println(fibonacciSeries2(2));
        System.out.println(fibonacciSeries2(4));
        System.out.println(fibonacciSeries2(8));

        System.out.println("\n------Task3------\n");
        System.out.println(Arrays.toString(findUniques(new int[]{1, 2, 3, 2}, new int[]{})));
        System.out.println(Arrays.toString(findUniques(new int[]{1, 2, 3, 4}, new int[]{3,4,5,5})));


        System.out.println("\n------Task4------\n");
        System.out.println(firstDuplicate(new int[]{1}));
        System.out.println(firstDuplicate(new int[]{1,2,2,3}));
        System.out.println(firstDuplicate(new int[]{1,2,3,3,4,1}));



        System.out.println("\n------Task5------\n");
        System.out.println(isPowerOf3(81));
        System.out.println(isPowerOf3(1));
        System.out.println(isPowerOf3(2));
        System.out.println(isPowerOf3(3));
        System.out.println(isPowerOf3(26));
    }

    public static int[] fibonacciSeries1(int n){
        int[] arr = new int[n];
        int start = 0;
        int second = 1;


        for (int i = 0; i < n; i++) {
            arr[i] = start;
            int next = start + second;
            start = second;
            second = next;

        }
        return arr;
    }

    public static int fibonacciSeries2(int n){
        int start = 0;
        int second = 1;


        for (int i = 1; i < n; i++) {
            int next = start + second;
            start = second;
            second = next;

        }
        return start;
    }

    public static int[] findUniques(int[] arr1, int[] arr2){
        Set<Integer> uniqueSet = new HashSet<>();

        for (int num : arr1) {
            uniqueSet.add(num);
        }

        for (int num : arr2) {
            uniqueSet.add(num);
        }

        int[] result = new int[uniqueSet.size()];
        int index = 0;

        for (int num : uniqueSet) {
            result[index++] = num;
        }

        return result;
    }

    public static int firstDuplicate(int[] arr) {
        Set<Integer> set = new HashSet<>();

        for (int num : arr) {
            if (set.contains(num)) {
                return num;
            } else {
                set.add(num);
            }
        }

        return -1;
    }



    public static boolean isPowerOf3(int num){

        return num  % 3 == 0 || num == 1;
    }
}
