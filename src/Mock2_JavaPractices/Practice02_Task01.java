package Mock2_JavaPractices;

import java.util.Random;

public class Practice02_Task01 {
    public static void main(String[] args) {

        Random random = new Random();

        int num = random.nextInt(51);

        if (num >= 10 && num <= 25){
            System.out.println(true);
        }
        else {
            System.out.println(false);
        }
    }
}
