package loops;

public class Exercise03_PrintEven {
    public static void main(String[] args) {
        //first way -> best way
        for (int i = 0; i <= 10; i++) {
            if (i % 2 == 0) System.out.println(i);
        }
        //second way
        for (int i = 0; i <= 10 ; i+=2) {
            System.out.println(i);
        }
        //third way
        for (int i = 0; i <=5; i++) {
            System.out.println(i * 2);
        }
    }
}
