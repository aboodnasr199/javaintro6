package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _03_Group_Methods {
    public static void main(String[] args) {
        ArrayList<String> group1Members = new ArrayList<>();
        group1Members.add("Belal");
        group1Members.add("Assem");
        group1Members.add("Gurkan");

        ArrayList<String> group2Members = new ArrayList<>();
        group2Members.add("Adam");
        group2Members.add("Melek");
        group2Members.add("Cihan");

        ArrayList<String> group3Members = new ArrayList<>();
        group3Members.add("Yousef");
        group3Members.add("Dima");
        group3Members.add("Sandina");

        System.out.println("Group 1 members = " + group1Members);
        System.out.println("Group 2 members = " + group2Members);
        System.out.println("Group 3 members = " + group3Members);

        /*
        create a new list called all members and store all the group members in that
         */

        System.out.println("\n------ADD ALL METHOD----\n");
        ArrayList<String> allMembers = new ArrayList<>();
       allMembers.addAll(group2Members);
       allMembers.addAll(group1Members);
       allMembers.addAll(group3Members);

        System.out.println("All members = " + allMembers);

        /*
        remove adam, assem, and yousef then print it
         */
        System.out.println("\n---- remove all() method----\n");
       ArrayList<String> elementToBeRemoved = new ArrayList<>();
       elementToBeRemoved.add("Adam");
        elementToBeRemoved.add("Assem");
        elementToBeRemoved.add("Yousef");

        allMembers.removeAll(elementToBeRemoved);

        System.out.println(allMembers);

        //allMembers.removeIf(element -> element.equals("Adam") || element.equals("Assem") || element.equals("Yousef"));
        System.out.println("\n-------Contains ALL method");
        ArrayList<String> check1 = new ArrayList<>(Arrays.asList("Cihan", "Dima", "Sandina"));
        ArrayList<String> check2 = new ArrayList<>(Arrays.asList("Jazzy", "Gurkan", "Melek"));

        System.out.println(check1);
        System.out.println(check2);

        System.out.println(allMembers.containsAll(check1));
        System.out.println(allMembers.containsAll(check2));

        System.out.println("\n-----retainAll method-----\n");
        ArrayList<String> keep = new ArrayList<>(Arrays.asList("Cihan", "Gurkan"));

        allMembers.retainAll(keep);

        System.out.println(allMembers);

        allMembers.removeIf(element -> !element.equals("Cihan") && !element.equals("Gurkan"));

        System.out.println(allMembers);


    }
}
