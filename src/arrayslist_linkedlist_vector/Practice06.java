package arrayslist_linkedlist_vector;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Practice06 {
    public static void main(String[] args) {
        System.out.println("\n------Task01------\n");
        System.out.println(Arrays.toString(double1(new int[]{3, 2, 5, 7, 0})));

        System.out.println("\n------Tas02-----\n");
        System.out.println(secondMax(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1))));
        System.out.println(secondMax(new ArrayList<>(Arrays.asList(5, 7, 2, 2, 10, 10))));

        System.out.println("\n------Tas03-----\n");
        System.out.println(secondMin(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1))));
        System.out.println(secondMin(new ArrayList<>(Arrays.asList(5, 7, 2, 2, 10, 10))));

        System.out.println("\n------Tas04-----\n");
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("Tech", "Global", "", null, "School"))));
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("", "", ""))));

        System.out.println("\n------Tas05-----\n");
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(200, 5, 100, 99, 101, 75))));
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(-12, -123, -5, 1000, 500, 0))));

        System.out.println("\n------Tas06-----\n");
        System.out.println(uniquesWords("TechGlobal School"));





    }

    public static int[] double1(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] *= 2;
        }
        return arr;
    }

    public static int secondMax(ArrayList<Integer> arr) {

        //collections way
        Collections.sort(arr);
        for (int i = arr.size() - 2; i >= 0; i--) {
            if (arr.get(i) < arr.get(i + 1)) return arr.get(i);

        }
        return 0;

        // for each loop with Integer.MIN_VALUE
        /*
        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;
        for (Integer element : arr) {
            if (element > max){
                secondMax = max;
                max = element;
            }

        }
        return secondMax;

         */
    }

    public static int secondMin(ArrayList<Integer> list) {
        Collections.sort(list);

        for (int i = 1; i <= list.size(); i++) {
            if (list.get(i) > list.get(0)) return list.get(i);

        }
        return 0;
    }

    public static ArrayList<String> removeEmpty(ArrayList<String> list) {
        //way1
        list.removeIf(element -> element == null || element.isEmpty());


        //way 2

        ArrayList<String> noEmpty = new ArrayList<>();
        for (String s : list) {
            if (s == null) continue;
            if (!s.isEmpty()) noEmpty.add(s);
        }
        return list;


    }
    public static ArrayList<Integer> remove3orMore(ArrayList<Integer> list){
        //way1
        list.removeIf(element -> element >= 100 || element <= -100);
        //return list;

        //way2
        ArrayList<Integer> noEmpty = new ArrayList<>();

        for (Integer s : list) {
            if (Math.abs(s) >= 100) noEmpty.add(s);

        }
        return list;
    }
    public static ArrayList<String> uniquesWords(String list){

        ArrayList<String> unique =new ArrayList<>();

        for (String s : list.split(" ")) {
            if (!unique.contains(s)) unique.add(s);

        }
        return unique;
    }
}
