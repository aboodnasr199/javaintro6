package Mock2_JavaPractices;

import utilities.ScannerHelper;

public class ReverseString {
    public static void main(String[] args) {
        String word = ScannerHelper.getString();
        System.out.println(reverseString(word));
    }
    public static String reverseString(String str){
        String a = "";

        for (int i = str.length()-1; i >= 0; i--) {
           a += str.charAt(i);


        }
        if (str.equals(a)){
            return "This is a pal"; 
        } else{
            return "This is not a pal";
        }

    }
}
