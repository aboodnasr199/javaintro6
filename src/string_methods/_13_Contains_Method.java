package string_methods;

import utilities.ScannerHelper;

import java.util.Locale;

public class _13_Contains_Method {
    public static void main(String[] args) {

        String name = "John Doe";

        boolean containsJohn = name.contains("John");
        System.out.println(containsJohn);

        String str = ScannerHelper.getString().trim();

        System.out.println(str.contains(" ") && str.endsWith("."));

    }
}
