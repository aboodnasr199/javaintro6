package methods;

import utilities.ScannerHelper;

import java.util.Scanner;

public class CheckName {
    public static void main(String[] args) {

        // i can invoked static methods with class name
        // i can call non-static methods with objects

        ScannerHelper scannerHelper = new ScannerHelper();

        String fName = ScannerHelper.getFirstName();
        String lName = ScannerHelper.getLastName();



        System.out.println("The full name is = " + fName + " " + lName);


    }
}



