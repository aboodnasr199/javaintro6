package string_methods;

public class _05_ToLowerCase_Method {
    public static void main(String[] args) {

        String str1 = "JAVA IS FUN";

        System.out.println(str1); // JAVA IS FUN

        System.out.println(str1.toLowerCase()); // java is fun

        char c = 'A';

        System.out.println(String.valueOf(c).toLowerCase()); // a
    }
}
