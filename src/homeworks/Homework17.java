package homeworks;

public class Homework17 {
    public static void main(String[] args) {
        System.out.println("\n----------TASK 1--------------\n");
        System.out.println(nthWord("I like programming languages", 2));
        System.out.println(nthWord("QA stands for Quality Assurance", 4));

        System.out.println("\n----------TASK 2--------------\n");
        System.out.println(isArmstrong(153));
        System.out.println(isArmstrong(123));

        System.out.println("\n----------TASK 3--------------\n");
        System.out.println(reverseNumber(371));
        System.out.println(reverseNumber(173));
        System.out.println(reverseNumber(12));


    }

    public static String nthWord(String sentence, int n) {

        String[] words = sentence.split("\\s+");


        if (n >= 1 && n <= words.length) {
            return words[n - 1];
        } else {
            return "";
        }
    }

    public static boolean isArmstrong(int number) {
        String strNumber = String.valueOf(number);
        int numDigits = strNumber.length();

        int sum = 0;
        int temp = number;

        while (temp != 0) {
            int digit = temp % 10;
            sum += Math.pow(digit, numDigits);
            temp /= 10;
        }

        return sum == number;
    }
    public static int reverseNumber(int number) {
        int reversedNumber = 0;

        while (number != 0) {
            int digit = number % 10;

            reversedNumber = reversedNumber * 10 + digit;

            number /= 10;
        }

        return reversedNumber;
    }
}
