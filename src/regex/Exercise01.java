package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise01 {
    public static void main(String[] args) {
        String input = ScannerHelper.getString();
        if (Pattern.matches("[a-zA-z0-9_-]{5,10}", input)){
            System.out.println("Valid Username");
        }else System.out.println("Error! Username must be " +
                "5 to 10  characters long and can only contain letters or numbers");
        //way2
        if (input.matches("[a-zA-z0-9_-]{5,10}")){
            System.out.println("Valid Username");
        }else System.out.println("Error! Username must be " +
                "5 to 10  characters long and can only contain letters or numbers");
    }
}
