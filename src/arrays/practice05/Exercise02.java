package arrays.practice05;

public class Exercise02 {
    public static void main(String[] args) {
        String[] arr = {"red", "blue", "yellow", "white"};
        getShortAndLong(arr);
    }

    public static void getShortAndLong(String[] arr) {
        String longest = arr[0];
        String shortest = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i].length() < shortest.length()) {
                shortest = arr[i];
            } else if (arr[i].length() > longest.length()) {
                longest = arr[i];
            }
        }
        System.out.println("The longest word is = " + longest);
        System.out.println("The shortest word is = " + shortest);

    }
}
