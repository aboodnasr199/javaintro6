package arrayslist_linkedlist_vector;

import java.util.Arrays;
import java.util.LinkedList;

public class _06_LinkedList {
    public static void main(String[] args) {

        LinkedList<String> cities = new LinkedList<>(Arrays.asList("Berlin", "Rome", "Kyiv", "Ankara", "Madrid", "Chicago"));

        System.out.println(cities.size()); // 6
        System.out.println(cities.contains("Miami")); // false
        System.out.println(cities.indexOf("Evanston")); // -1

        System.out.println(cities.getFirst()); // berlin
        System.out.println(cities.getLast()); // chicago

        System.out.println(cities.removeFirst()); // berlin
        System.out.println(cities.removeLast()); // chicago

        System.out.println(cities);

        System.out.println(cities.pop()); // rome = removefirst

        System.out.println(cities);

        cities.push("Barcelona");

        System.out.println(cities);

        System.out.println(cities.offer("Gent"));

        System.out.println(cities);
    }
}
