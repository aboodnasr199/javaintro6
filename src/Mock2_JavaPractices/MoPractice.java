package Mock2_JavaPractices;

import java.io.FilterOutputStream;
import java.util.Arrays;

public class MoPractice {
    public static void main(String[] args) {
        String[] arr = {"car", "pizza", "diza", "iza", "iz"};
        int count = 0;
        for(String element : arr){
            if (element.contains("iza")){
                System.out.println("1: " + element.replaceAll("iza", "x"));
                System.out.println("2: " + element);
                element = element.replaceAll("iza", "x");
                System.out.println(element);
                count++;
            }

        }
        System.out.println(Arrays.toString(arr));
        System.out.println(count);
    }
}
