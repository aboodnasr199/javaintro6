package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {

        System.out.println("\n-------Task1--------\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 3 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();

        System.out.println("The product of the numbers entered is = " + (num1)*(num2)*(num3));

        System.out.println("\n-------Task2--------\n");

        String firstName, lastName;
        int yearOfBirth = 2000, currentYear = 2023;

        System.out.println("What is your first name?");
        firstName = input.next();

        System.out.println("What is your last name?");
        lastName = input.next();


        System.out.println("What is your year of birth?");
        yearOfBirth = input.nextInt();

        System.out.println("Adam Nasr's age is = " + (currentYear - yearOfBirth));





        System.out.println("\n-------Task3--------\n");

        System.out.println("What is your full name?");
        String fullName = input.next();

        System.out.println("What is your weight?");
        input.nextLine();
        int weight = input.nextInt();
        input.nextLine();

        System.out.println("Adam Nasr's weight is = " + weight * 2.205 + " lbs.");



        System.out.println("\n-------Task4--------\n");


        System.out.println("What is your full name?");
        String fullName1 = input.nextLine();


        System.out.println("What is your age?");
        int age1 = input.nextInt();
        input.nextLine();


        System.out.println("What is your full name?");
        String fullName2 = input.nextLine();

        System.out.println("What is your age?");
        int age2 = input.nextInt();
        input.nextLine();


        System.out.println("What is your full name?");
        String fullName3 = input.nextLine();

        System.out.println("What is your age?");
        int age3 = input.nextInt();

        System.out.println(fullName1 + "'s age is " + age1);
        System.out.println(fullName2 + "'s age is " + age2);
        System.out.println(fullName3 + "'s age is " + age3);
        System.out.println("The average age is " + (age1 + age2 + age3) / 3);
        System.out.println("The eldest age is " + age1);
        System.out.println("The youngest age is " + age2);



    }
}
