package Mock2_JavaPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class Practice06_Task04 {
    public static void main(String[] args) {
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("Tech", "Global", "", null, "", "School"))));
    }
public static String removeEmpty(ArrayList<String> list){
    for (int i = 0; i < list.size(); i++) {
        list.removeIf(element -> element == null || element.isEmpty());


    }
    return list.toString();
}
}
