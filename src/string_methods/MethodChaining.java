package string_methods;

public class MethodChaining {
    public static void main(String[] args) {

        String str = "TechGlobal";

        //Single method

        System.out.println(str.toLowerCase()); // techglobal

        //2 methods chained

        System.out.println(str.toLowerCase().contains("tech")); //true

        // 3 methods chained

        System.out.println(str.toUpperCase().substring(4).length());

        // 4 methods chained
    String sentence = "Hello, my name is John Doe. I am 30 years old and i to to school at TechGlobal";
        System.out.println(sentence.toLowerCase().replace("a", "X")
                .replace("e", "X")
                .replace("i.", "X")
                .replace("o", "X")
                .replace("u", "X"));
    }
}
