package homeworks;

import utilities.ScannerHelper;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("\n--------Task1--------\n");
        System.out.println(countConsonants(new String("hello")));
        System.out.println(countConsonants(new String("JAVA")));
        System.out.println(countConsonants(new String("")));

        System.out.println("\n--------Task2--------\n");

        System.out.println(Arrays.toString(wordArray("java is  fun")));
        System.out.println(Arrays.toString(wordArray("Hello, nice to meet you!!")));
        System.out.println(Arrays.toString(wordArray("hello")));

        System.out.println("\n--------Task3--------\n");
        System.out.println(removeExtraSpaces("hello"));
        System.out.println(removeExtraSpaces("java is   fun"));

        System.out.println("\n--------Task4--------\n");
        System.out.println(count3OrLess());

        System.out.println("\n--------Task5--------\n");
        System.out.println(isDateFormatValid(new String("01/21/1999")));
        System.out.println(isDateFormatValid(new String("1/20/1991")));

        System.out.println("\n--------Task6--------\n");
        System.out.println(isEmailFormatValid(new String("abc@gmail.com")));
        System.out.println(isEmailFormatValid(new String("abc@student.techglobal.com")));
        System.out.println(isEmailFormatValid(new String("a@gmail.com")));




    }
    public static int countConsonants(String str){
        str = str.replaceAll("[aeiouAEIOU]", "");

        return str.length();

    }
    public static String[] wordArray(String str){

        String[] words = str.split("\\W+");
        words = Arrays.stream(words).filter(element -> element.length() > 0).toArray(String[]::new);

        return words;

        //belals way
        //str = str.replaceAll("[ ]{1,}", " ");
        //str = str.replaceAll("[^a-zA-Z ]", "");
        // return str.split(" ")

    }
    public static String removeExtraSpaces(String str){
        String result = str.replaceAll("\\s+", " ");

        // \\s -> matches single white space chars \\s+ -> matches sequence of one or more whitespace chars
        result = result.trim();

        return result;
    }
    public static int count3OrLess(){
        //bilals way
        String str = ScannerHelper.getString();

      str = str.replaceAll("[^a-zA-Z ]", "").trim();
      String[] arr = str.split("\\s+");

      int count = 0;

      for(String s: arr){
          if(!str.contains(" "))
      if(s.length()>3)
      count++;
      }
      return count;


    }
    public static boolean isDateFormatValid(String dateOfBirth){
        return dateOfBirth.matches("^\\d{2}/\\d{2}/\\d{4}$");
    }
    public static boolean isEmailFormatValid(String email){
        return email.matches("[a-zA-Z0-9$%#!.]{2,}@[a-zA-Z_.-]{2,}.[a-zA-Z]{2,}");
    }
}
