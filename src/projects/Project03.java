package projects;

import java.util.Random;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n------Task1-----\n");

        String s1 = "24", s2 = "5";
        int sum = Integer.parseInt(s1) + Integer.parseInt(s2);
        int subtraction = Integer.parseInt(s1) - Integer.parseInt(s2);
        int division = Integer.parseInt(s1) / Integer.parseInt(s2);
        int multiplication = Integer.parseInt(s1) * Integer.parseInt(s2);
        int remainder = Integer.parseInt(s1) % Integer.parseInt(s2);

        System.out.println("The sum of " + (Integer.parseInt(s1) + " and " + Integer.parseInt(s2) +
                " is = " + sum));

        System.out.println("The subtraction of " + (Integer.parseInt(s1) + " and " + Integer.parseInt(s2) +
                " is = " + subtraction));

        System.out.println("The division of " + (Integer.parseInt(s1) + " and " + Integer.parseInt(s2) +
                " is = " + division));

        System.out.println("The multiplication of " + (Integer.parseInt(s1) + " and " + Integer.parseInt(s2) +
                " is = " + multiplication));

        System.out.println("The remainder of " + (Integer.parseInt(s1) + " and " + Integer.parseInt(s2) +
                " is = " + remainder));

        System.out.println("\n------Task2-----\n");

        Random r1 = new Random();

        int number = r1.nextInt(36);


        if (number == 2 && number == 3 && number == 5 && number == 7 && number == 11 && number == 13 &&
                number == 17 && number == 19 && number == 23 && number == 29 && number == 31){
            System.out.println(number + " IS A PRIME NUMBER");
        }
        else {
            System.out.println(number + " IS NOT A PRIME NUMBER");
        }



        System.out.println("\n------Task3-----\n");

        Random r2 = new Random();

        int num1 = r2.nextInt(51);
        int num2 = r2.nextInt(51);
        int num3 = r2.nextInt(51);

        System.out.println("Lowest number = " + Math.min(Math.min(num1, num2), num3));
        System.out.println("Middle number = " + (num1 + num2 + num3) / 3);
        System.out.println("Greatest number = " + Math.max(Math.max(num1, num2), num3));



        System.out.println("\n------Task4-----\n");

        char c = 'a';

        int character1 = c;




        if (character1 >= 48 && character1 <= 57) {
            System.out.println("Invalid character detected!!!");
        } else if (character1 >= 65 && character1 <= 90) {
            System.out.println("The letter is uppercase");
        } else if (character1 >= 97 && character1 <= 122) {
            System.out.println("The letter is lowercase");

        }


        System.out.println("\n------Task5-----\n");

        char c1 = 'c';

        int character = c1;

        System.out.println(character);


        if (character >= 32 && character <= 47) {
            System.out.println("Invalid character detected");
        } else if (character == 97 || character == 101 || character == 108 || character == 111 || character == 117
                || character == 65 || character == 69 || character == 73 ||
                character == 79 || character == 85) {
            System.out.println("The letter is a vowel");
        } else {
            System.out.println("The letter is a consonant");
        }


        System.out.println("\n------Task6-----\n");

        char c2 = '*';

        int character2 = c2;

        if (character2 >= 48 && character2 <= 57 || character2 >= 65 && character2 <= 90
                || character2 >= 97 && character2 <= 122){
            System.out.println("Invalid character detected");
        }
        else {
            System.out.println("Special character is = " + character2);
        }


        System.out.println("\n------Task7-----\n");

        char c3 = '@';

        int character3 = c3;

        if (character3 >= 65 || character3 <= 90 && character3 >= 90 && character3 <= 122) {
            System.out.println("Character is a letter");
        }
        else if (character3 >= 48 && character3 <= 57){
            System.out.println("Character is a digit");
        }
        else {
            System.out.println("Character is a special character");
        }
    }
}


