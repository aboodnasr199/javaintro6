package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Exercise03_RemoveElements {
    public static void main(String[] args) {
        System.out.println(removeVowels(new ArrayList<>(Arrays.asList("Hello", "World"))));
        System.out.println(removeVowels(new ArrayList<>(Arrays.asList("abc", "A", "123"))));
        System.out.println(removeVowels(new ArrayList<>(Arrays.asList("xyz", "tt", "    "))));

        System.out.println(removeVowels2(new ArrayList<>(Arrays.asList("Hello", "World"))));
        System.out.println(removeVowels2(new ArrayList<>(Arrays.asList("abc", "A", "123"))));
        System.out.println(removeVowels2(new ArrayList<>(Arrays.asList("xyz", "tt", "    "))));

        System.out.println(removeVowels3(new ArrayList<>(Arrays.asList("Hello", "World"))));
        System.out.println(removeVowels3(new ArrayList<>(Arrays.asList("abc", "A", "123"))));
        System.out.println(removeVowels3(new ArrayList<>(Arrays.asList("xyz", "tt", "    "))));

    }

    public static ArrayList<String> removeVowels(ArrayList<String> arr) {
        arr.removeIf(element -> element.toLowerCase().contains("i") || element.toLowerCase().contains("a")
                || element.toLowerCase().contains("o") || element.toLowerCase().contains("e")
                || element.toLowerCase().contains("u"));
        return arr;
    }
    public static ArrayList<String> removeVowels2(ArrayList<String> list){
        ArrayList<String> result = new ArrayList<>();
        for (String element : list) {
            if (!element.toLowerCase().contains("a") && !element.toLowerCase().contains("e")
                    && !element.toLowerCase().contains("i") && !element.toLowerCase().contains("o")
                    && !element.toLowerCase().contains("u")) result.add(element);

        }
        return result;
    }
    public static ArrayList<String> removeVowels3(ArrayList<String> list){

        Iterator<String> iterator = list.iterator();

        while (iterator.hasNext()){
            String currentElement = iterator.next();
            if (currentElement.toLowerCase().contains("a") ||
                    currentElement.toLowerCase().contains("e") ||
                    currentElement.toLowerCase().contains("i")  ||
                    currentElement.toLowerCase().contains("o") ||
                    currentElement.toLowerCase().contains("u")) iterator.remove();
        }
        return list;
    }



    }

