package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class _05_ArrayToArrayList {
    public static void main(String[] args) {

        /*
        How to convert an array to array list
        1. asList
        2. Loops - manual way
        3. Collections.addAll(Collection1, collection2); // it will add collection2 into collection1
         */
        System.out.println("\n------ Way 1 asList-----\n");
        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Berlin", "Paris", "Rome"));

        System.out.println("\n------ Way 2 loops-----\n");
        String[] countries = {"USA", "Germany", "Spain", "Italy"};

        ArrayList<String> list = new ArrayList<>();

        for (String country : countries) {
            list.add(country);


        }
        System.out.println(list);

        System.out.println("\n------ Way 3 collection.add all-----\n");

        Collections.addAll(list, countries);

        System.out.println(list);


    }
}
