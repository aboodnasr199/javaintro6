package string_methods;

public class _10_Trim_Method {
    public static void main(String[] args) {
        /*
        1. return type
        2. returns a string
        3. non-static
        4. NO arguments
         */
        String str = "TechGlobal      ";

        System.out.println(str.trim());

        String str1 = "t e c h g l o b a l";

        System.out.println(str1.trim());
    }
}
