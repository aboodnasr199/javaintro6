package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _07_ArrayList_To_Array {
    public static void main(String[] args) {
        System.out.println(uniques(new int[] {3, 5, 7, 3, 5}));
        System.out.println(uniques(new int[] {10, 10, 10, 10}));
        System.out.println(uniques(new int[] {13, 20, 20, 13}));
        System.out.println(uniques(new int[] {}));


    }
    public static Integer[] uniques(int[] arr){
        ArrayList<Integer> uniqueNums = new ArrayList<>();

        for (int j : arr) {
            if (!uniqueNums.contains(j)) {
                uniqueNums.add(j);
            }
        }
        return uniqueNums.toArray(new Integer[0]);

    }


}
