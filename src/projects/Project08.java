package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("\n------Task01------\n");
        System.out.println(findClosestDistance(new int[]{4}));
        System.out.println(findClosestDistance(new int[]{4, 8, 7, 15}));
        System.out.println(findClosestDistance(new int[]{10, -5, 20, 50, 100}));

        System.out.println("\n------Task02------\n");
        System.out.println(findSingleNumber(new int[] {2}));
        System.out.println(findSingleNumber(new int[] {5,3,-1,3,5,100,-1}));

        System.out.println("\n------Task03------\n");
        System.out.println(findFirstUniqueCharacter(new String("Hello")));
        System.out.println(findFirstUniqueCharacter(new String("abc abc d")));

        System.out.println("\n------Task04------\n");
        System.out.println(findMissingNumber(new int[]{2, 4}));
        System.out.println(findMissingNumber(new int[]{2, 3,1,5}));
        System.out.println(findMissingNumber(new int[]{4, 7,8,6}));


    }
   public static int findClosestDistance(int[] arr){
        if(arr.length < 2){
            return -1;
        }
       Arrays.sort(arr);
        int difference = Integer.MAX_VALUE;
       for (int i = 1; i < arr.length; i++) {
           int dif = arr[i] - arr[i-1];
           if (dif < difference){
               difference = dif;
           }

       }
       return difference;
   }
   public static int findSingleNumber(int[] arr){
       //way-1
        int result = 0;

       for (int j : arr) {
         result ^= j;

       }
       return result;


        /*
      WAY-2
        Arrays.sort(arr);
      \
       for (int i = 0; i < arr.length-1; i++) {
           if (arr[i] != arr[i]){
               return arr[i];
           }

       }
       return arr[arr.length-1];

       */
   }
   public static char findFirstUniqueCharacter(String str){

       for (int i = 0; i < str.length(); i++) {
           char c = str.charAt(i);
           if (str.indexOf(c) == str.lastIndexOf(c)){
               return c;
           }

       }
       return 0;
   }
   public static int findMissingNumber(int[] arr){
        if (arr.length < 2){
            return 0;
        }
        Arrays.sort(arr);
       for (int i = 0; i < arr.length; i++) {
           if (arr[i+1] - arr[i] != 1){
               return arr[i] + 1;
           }
       }
       return 0;
   }
}
