package git_demo;

public class MergeConflict {
    public static void main(String[] args) {
        String favColor = "blue";
        int shades = 7;

        System.out.println("My favorite color is " + favColor + " and theres " + shades + " shades of blue.");
    }
}
