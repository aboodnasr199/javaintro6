package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n-------Task1-----\n");

        System.out.println("Please enter number 1");
         int number1 = input.nextInt();
        System.out.println("The number 1 entered by user is = " + number1);

        System.out.println("Please enter number 2");
        int number2 = input.nextInt();
        System.out.println("The number 2 entered by user is = " + number2);

        int sum1 = number1 + number2;
        System.out.println("The sum of number 1 and 2 by user is = " + sum1);

        System.out.println("\n-------Task2-----\n");

        System.out.println("Please enter 2 numbers: ");
        int product = input.nextInt();
        int product2 = input.nextInt();

        int productFinal = product * product2;

        System.out.println("The product of the given 2 numbers is: " + productFinal);


        System.out.println("\n-------Task3-----\n");

        System.out.println("Please enter the 2 floating numbers");
        double num1 = 24, num2 = 10;
        num1 = input.nextDouble();
        num2 = input.nextDouble();

        double sum = (num1 + num2);
        double multiplication = (num1 * num2);
        double subtraction = (num1 - num2);
        double division = (num1 / num2);
        double remainder = (num1 % num2);

        System.out.println("The sum of the given numbers is = " + sum);

        System.out.println("The product of the given numbers is = " + multiplication);

        System.out.println("The subtraction of the given numbers is = " + subtraction);

        System.out.println("The division of the given numbers is = " + division);

        System.out.println("The remainder of the given numbers is = " + remainder);

        System.out.println("\n-------Task4-----\n");

        int a = (-10 + 7 * 5);
        int b = (72 + 24) % 24;
        int c = (10 + -3*9 / 9);
        int d = (5 + 18 / 3 * 3 - (6 % 3));

        System.out.println("1. " + a);
        System.out.println("2. " + b);
        System.out.println("3. " + c);
        System.out.println("4. " + d);

        System.out.println("\n-------Task5-----\n");

        int b1 = 7, b2 = 11;

        System.out.println("Please enter 2 numbers");
        b1 = input.nextInt();
        b2 = input.nextInt();

        int addition = b1 + b2;
        int average = addition / 2;

        System.out.println("The average of the given numbers is: " + average);

        System.out.println("\n-------Task6-----\n");

        int c1 = 6, c2 = 10, c3 = 12, c4 = 15, c5 = 17;

        System.out.println("Please enter 5 numbers");
        c1 = input.nextInt();
        c2 = input.nextInt();
        c3 = input.nextInt();
        c4 = input.nextInt();
        c5 = input.nextInt();

        int average1 = (c1 + c2 + c3 + c4 + c5) / 5;

        System.out.println("The average of the given numbers is: " + average1);


        System.out.println("\n-------Task7-----\n");

        int a1 = 5, a2 = 6, a3 = 10;
        System.out.println("Please enter number 1 ");
        a1 = input.nextInt();

        System.out.println("Please enter number 2 ");
        a2 = input.nextInt();

        System.out.println("Please enter number 3 ");
        a3 = input.nextInt();

        System.out.println("The 5 multiplied with 5 is = " + a1 * a1);

        System.out.println("The 6 multiplied with 6 is = " + a2 * a2);

        System.out.println("The 10 multiplied with 10 is = " + a3 * a3);

        System.out.println("\n-------Task8-----\n");

        System.out.println("Please enter the side of a square");
        input.nextInt();

        int z1 = 7;

        int area = z1 * z1;

        int perimeter = (4 * z1);

        System.out.println("Perimeter of the square = " + perimeter);

        System.out.println("Area of the square = " + area);

        System.out.println("\n-------Task9-----\n");

        double yearly = 90000;

        double year3 = 90000 * 3;

        System.out.println("A software engineer in Test can earn " + year3 + " in 3 years");



        System.out.println("\n-------Task10-----\n");

        String favBook, favColor;
        int favNumber;

        System.out.println("Please enter your favorite book");
        favBook = input.next();
        input.nextLine();


        System.out.println("Please enter your favorite color");
        favColor = input.nextLine();


        System.out.println("Please enter your favorite number");
       favNumber = input.nextInt();

        System.out.println("User's favorite book is: " + favBook + " 101");
        System.out.println("User's favorite color is: " + favColor);
        System.out.println("User's favorite number is: " + favNumber);


        System.out.println("\n-------Task11-----\n");

        String firstName, lastName, age, emailAddress, phoneNumber, address;

        System.out.println("Please enter your first name");
        firstName = input.next();
        input.nextLine();

        System.out.println("Please enter your last name");
        lastName = input.nextLine();

        System.out.println("Please enter your age");
        age = input.nextLine();

        System.out.println("Please enter your email address");
        emailAddress = input.nextLine();

        System.out.println("Please enter your phone number");
        phoneNumber = input.nextLine();

        System.out.println("Please enter your address");
        address = input.nextLine();

        System.out.println("User who joined this program is " + firstName + " " + lastName + ". " + firstName + "'s age is \n" +
                age + ". " + firstName + "'s email address is " + emailAddress + ". Phone number \nis " + phoneNumber
                    + ", and address is " + address + ".");


    }
}