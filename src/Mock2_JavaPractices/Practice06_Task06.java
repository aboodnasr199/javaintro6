package Mock2_JavaPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class Practice06_Task06 {
    public static void main(String[] args) {
        System.out.println(uniquesWords("Star Light Star Bright"));
        System.out.println(uniquesWords("TechGLobal School"));


    }


        public static ArrayList<String> uniquesWords(String str){

            ArrayList<String> newList = new ArrayList<>();
            for (String s : str.split(" ")) {

                if (!newList.contains(s)) newList.add(s);
            }
            return newList;
        }

    }




