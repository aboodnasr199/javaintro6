package variables;

public class CreatingMultipleVariables {
    public static void main(String[] args) {

        int age1;
        int age2;
        int age3;
        // or int age1;, int age2;, int age3;

        age1 = 15;
        age2 = 20;
        age3 = 25;

        System.out.println(age1);
        System.out.println(age2);
        System.out.println(age3);

        double d1;
        double d2;
        double d3;

        d1 = 25.6;
        d2 = 30.7;
        d3 = 35.8;

        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);

        String s1, s2, s3;

        String str;
        int num;



    }
}
