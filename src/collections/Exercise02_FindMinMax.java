package collections;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.TreeSet;

public class Exercise02_FindMinMax {
    public static void main(String[] args) {
        System.out.println(max(new Integer[]{10, 100, 123, 507, 25}));

        System.out.println(min(new Integer[]{10, 100, 123, 507, 25}));

        System.out.println(secondMax(new Integer[]{10, 100, 123, 507, 25}));



    }


    public static int max(Integer[] numbers){

        return new TreeSet<>(Arrays.asList(numbers)).last();

    }

    public static int min(Integer[] numbers){

        return new TreeSet<>(Arrays.asList(numbers)).first();

    }

    public static int secondMax(Integer[] numbers){
    if (numbers.length < 2 ) throw new NoSuchElementException("Size is " + numbers.length);
    else {
       TreeSet<Integer> set = new TreeSet<>(Arrays.asList(numbers));
       set.remove(set.last()); // remove the maximum element

       return set.last(); //the new last element is the second maximum
    }

    }

    public static int secondMin(Integer[] numbers){
        if (numbers.length < 2 ) throw new NoSuchElementException("Size is " + numbers.length);
        else {
            return 0;
        }

    }
}
