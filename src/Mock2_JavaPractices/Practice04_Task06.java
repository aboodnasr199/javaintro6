package Mock2_JavaPractices;

import utilities.ScannerHelper;

public class Practice04_Task06 {
    public static void main(String[] args) {
        System.out.println(more100());
    }


    public static String more100(){

        int num = 0;
        int sum =0;
        boolean isFirst = true;
        do {
            num = ScannerHelper.getNumber();
            if (num >= 100 && isFirst)
                return "This number is already 100 or more";
            sum += num;
            System.out.println(sum);
            isFirst = false;
        }while (sum < 100);
        return "The sum of given numbers is = " + sum;


    }


}
