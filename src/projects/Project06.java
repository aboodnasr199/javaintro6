package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Project06 {
    public static void main(String[] args) {
        System.out.println("\n-----Task 1-----\n");
        int[] nums = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(nums);

        System.out.println("\n-----Task 2-----\n");
        findGreatestAndSmallestWithoutSort(nums);

        System.out.println("\n-----Task 3-----\n");
        int[] numbers = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(numbers);

        System.out.println("\n-----Task 4-----\n");
        findSecondGreatestAndSmallestWithout(numbers);

        System.out.println("\n-----Task 5-----\n");
        String[] arr = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElementsInAnArray(arr);

        System.out.println("\n-----Task 6-----\n");
        String[] sentence = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementInAnArray(sentence);
    }

    public static void findGreatestAndSmallestWithSort(int[] nums) {
        Arrays.sort(nums);
        System.out.println("Smallest number is = " + nums[0]);
        System.out.println("Greatest number is = " + nums[nums.length - 1]);

    }

    public static void findGreatestAndSmallestWithoutSort(int[] nums) {
        int max = nums[0];
        int min = nums[0];

        for (int num : nums) {
            if (num > max) max = num;
            else if (num < min) num = min;

        }
        System.out.println("The min number is = " + min);
        System.out.println("The max number is = " + max);
    }

    public static void findSecondGreatestAndSmallestWithSort(int[] numbers) {
        Arrays.sort(numbers);
        int min = numbers[0];
        int max = numbers[numbers.length - 1];

        int secondSmallest = 0;
        int secondBiggest = 0;

        for (int number : numbers) {
            if (number > min) {
                secondSmallest = number;
                break;
            }
        }
        for (int i = numbers.length - 1; i > 0; i--) {
            if (numbers[i] < max) {
                secondBiggest = numbers[i];
                break;
            }

        }
        System.out.println("Second smallest is = " + secondSmallest);
        System.out.println("Second greatest is = " + secondBiggest);
    }

    public static void findSecondGreatestAndSmallestWithout(int[] numbers) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        int secondGreatest = Integer.MIN_VALUE;
        int secondSmallest = Integer.MAX_VALUE;

        for (int number : numbers) {
            if (number < min) {
                min = number;
            } else if (number < secondSmallest && number != min) {
                secondSmallest = number;
            }

            if (number > max) {
                secondGreatest = max;
                max = number;
            } else if (number > secondGreatest && number != max) {
                secondGreatest = number;
            }
        }
        System.out.println("Second smallest is = " + secondSmallest);
        System.out.println("Second greatest is = " + secondGreatest);
    }

    public static void findDuplicatedElementsInAnArray(String[] arr) {
        ArrayList<String> list = new ArrayList<>();
        ArrayList<String> duplicates = new ArrayList<>();

        for (String s : arr) {
            if (list.contains(s)) {
                if (!duplicates.contains(s)) {
                    duplicates.add(s);
                }
            } else {
                list.add(s);
            }
        }
        for (String duplicate : duplicates) {
            System.out.println(duplicate);

        }
    }

    public static void findMostRepeatedElementInAnArray(String[] elements) {
        ArrayList<String> list = new ArrayList<String>();
        for (String element : elements) {
            list.add(element);
        }

        String mostRepeat = null;
        int count = 0;

        for (int i = 0; i < list.size(); i++) {
            int count1 = 0;
            for (int j = 0; j < list.size(); j++) {
                if (list.get(i).equals(list.get(j))) {
                    count1++;
                }
            }
            if (count1 > count) {
                mostRepeat = list.get(i);
                count = count1;
            }
        }

        System.out.println("The most repeated element is: " + mostRepeat);
    }
}








