package Mock2_JavaPractices;

import java.util.Random;

public class Practice01_Task05 {
    public static void main(String[] args) {
        Random random = new Random();

        int num1 = random.nextInt(25) + 1;
        int num2 = random.nextInt(27) + 23;
        int num3 = random.nextInt(52) + 23;
        int num4 = random.nextInt(77) + 23;

        System.out.println("Max num = " + Math.max(Math.max(num1, num2),Math.max(num3, num4)));
        System.out.println("Min num = " + Math.min(Math.min(num1, num2),Math.min(num3, num4)));

        System.out.println(Math.abs(num1 - num2));
        System.out.println(Math.abs(num3 - num4));

        System.out.println(Math.abs(num2- num3));

        System.out.println("Average = " + (num1 + num2 + num3 + num4) / 4);

    }
}
