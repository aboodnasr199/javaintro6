package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;

public class Homework07 {
    public static void main(String[] args) {
        System.out.println("\n-----Task 1-----\n");

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));
        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        System.out.println("\n-----Task 2-----\n");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));
        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n-----Task 3-----\n");

        ArrayList<Integer> sorted = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));
        System.out.println(sorted);
        Collections.sort(sorted);
        System.out.println(sorted);

        System.out.println("\n-----Task 4-----\n");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n-----Task 5-----\n");

        ArrayList<String> marvel = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panther", "Deadpool", "Captin America"));
        System.out.println(marvel);

        if (marvel.contains("Wolverine")) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        System.out.println("\n-----Task 6-----\n");

        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captin America", "Iron Man"));
        Collections.sort(avengers);
        System.out.println(avengers);

        if (avengers.contains("Hulk") && avengers.contains("Iron Man")) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        System.out.println("\n-----Task 7-----\n");

        ArrayList<Character> chars = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));
        System.out.println(chars);

        for (Character element : chars) {
            System.out.println(element);

        }

        System.out.println("\n-----Task 8-----\n");

        ArrayList<String> objects = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));

        System.out.println(objects);
        Collections.sort(objects);
        System.out.println(objects);

        int count = 0;
        int noCount = 0;
        for (String element : objects) {
            if (element.toLowerCase().startsWith("m")) {
                count++;
            }
            if (!element.toLowerCase().contains("a") && !element.toLowerCase().contains("e")) {
                noCount++;
            }
        }
        System.out.println(count);
        System.out.println(noCount);

        System.out.println("\n-----Task 9-----\n");

        ArrayList<String> objects2 = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));
        System.out.println(objects2);

        int uppers = 0;
        int lowers = 0;
        int hasP = 0;
        int startEndP = 0;

        for (String element : objects2) {
            if (Character.isUpperCase(element.charAt(0))) {
                uppers++;
            } else {
                lowers++;
            }
            if (element.startsWith("P") || element.endsWith("p")) {
                startEndP++;
            }

            if (element.toLowerCase().contains("p")) {
                hasP++;

            }
        }
        System.out.println("Elements start with uppercase = " + uppers);
        System.out.println("Elements start with lowercase = " + lowers);
        System.out.println("Elements having p or P = " + hasP);
        System.out.println("Elements starting or ending with P or p = " + startEndP);

        System.out.println("\n-----Task 10-----\n");

        ArrayList<Integer> numbers2 = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));
        System.out.println(numbers2);
        int by10 = 0;
        int even15 = 0;
        int odd20 = 0;
        int lessGreat = 0;

        for (Integer element : numbers2) {
            if (element % 10 == 0) {
                by10++;
            }
            if (element % 2 == 0 && element > 15) {
                even15++;
            }
            if (element % 2 == 1 && element < 20) {
                odd20++;
            }
            if (element < 15 || element > 50) {
                lessGreat++;
            }


        }
        System.out.println("Elements that can be divided by 10 = " + by10);
        System.out.println("Elements that are even and greater than 15 = " + even15);
        System.out.println("Elements that are odd and less than 20 = " + odd20);
        System.out.println("Elements that are less than 15 or greater than 50 = " + lessGreat);


    }
}

