package Mock2_JavaPractices;

import java.util.Random;

public class Practice01_Task02 {
    public static void main(String[] args) {
        Random random = new Random();

        int num1 = random.nextInt(10) + 1;
        int num2 = random.nextInt(10) + 1;

        System.out.println("Min number = " + Math.min(num1, num2));
        System.out.println("Max number = " + Math.max(num1, num2));
        System.out.println("Difference is = " + Math.abs(num1 - num2));
    }
}
