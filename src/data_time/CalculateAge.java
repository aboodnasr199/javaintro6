package data_time;

import java.time.LocalDate;
import java.util.Scanner;

public class CalculateAge {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your year of birth: ");
        int age = input.nextInt();

        System.out.println(LocalDate.now().getYear() - age);
    }
}
