package loops.do_while_loop;

import utilities.ScannerHelper;

public class Exercise01_GetNumberDividedBy5 {
    public static void main(String[] args) {

       int number;
       do {
           number = ScannerHelper.getNumber();
       }
       while (number % 5!= 0);

        System.out.println("End of program");

        System.out.println("\n-----while loop----\n");
        while (true) {
            int n1 = ScannerHelper.getNumber();
            if (n1 % 5 == 0) System.out.println("End of Program");
            break;
        }
        System.out.println("\n-----for i loop-----\n");

        for (; ;) {
           int n2 =ScannerHelper.getNumber();
           if (n2 % 5 == 0) System.out.println("End of program");
           break;

        }

    }
}
