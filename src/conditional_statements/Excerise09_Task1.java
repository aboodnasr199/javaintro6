package conditional_statements;

import java.util.Random;

public class Excerise09_Task1 {
    public static void main(String[] args) {

        Random r1 = new Random();
        int num1 = r1.nextInt(51);

        if (num1 >= 10 && num1 <= 25){
            System.out.println(true);
        }
        else {
            System.out.println(false);
        }


        int num2 = r1.nextInt(100) + 1;


        if (num2 <= 25){
            System.out.println(num2 + " is in the 1st half");
            System.out.println(num2 + " is in the 1st quarter");
        }
        else if (num2 <= 50){
            System.out.println(num2 + " is in the 1st half");
            System.out.println(num2 + " is in the 2nd quarter");
        }
        else if (num2 <= 75){
            System.out.println(num2 + " is in the 2nd half");
            System.out.println(num2 + " is in the 3rd quarter");
        }
        else {
            System.out.println(num2 + " is in the 2nd half");
            System.out.println(num2 + " is in the 4th quarter");
        }
       /*
        Requirement:
        -Assume you are given a single character. (It will be hard-coded)
        -If given char is a letter, then print “Character is a letter”
        -If given char is a digit, then print “Character is a digit”
        USE ASCII TABLE for this task
        Test data:
        ‘v’
        Expected result:
        Character is a letter
        Test data:
        ‘5’
        Expected result:
        Character is a digit

        */

        char character = 'a';

        if ((character >= 65 && character <= 90) || character >= 97 && character <= 122){
            System.out.println("Character is a letter");
        }
        else if (character >= 48 && character <= 57){
            System.out.println("Character is a digit");

        }

    }
}


