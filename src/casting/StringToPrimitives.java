package casting;

public class StringToPrimitives {
    public static void main(String[] args) {
        int num1 =5, num2 = 10;

        System.out.println(num1 + num2); // returns 15 - numeric 15
        System.out.println("" + num1 + num2); // returns 510 - text
        System.out.println("" + (num1 + num2)); // returns "15" - text 15



        // converting primitives to strings
        System.out.println("" + num1 + num2); // 510
        // another way of doing above line
        System.out.println(String.valueOf(num1) + String.valueOf(num2)); // returns 510 in "text"

        System.out.println(String.valueOf(num2 + num1) + (7 + num2)); // returns 1517 text

        // converting strings to primitives

        String price = "1597.06";
        System.out.println(Double.valueOf(price) - 10); // returns 1587.06


    }
}
