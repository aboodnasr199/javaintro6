package arrays.practice05;

import java.util.Arrays;

public class Exercise06 {
    public static void main(String[] args) {
        duplicated("baNana");
    }

    public static void duplicated(String str){
        String answer = "";
        for (int i = 0; i < str.length(); i++) {
            for (int j = i + 1; j < str.length(); j++) {
            if(str.toLowerCase().charAt(i) == str.toLowerCase().charAt(j)
                    && !answer.contains(""+str.charAt(i))) answer += str.charAt(i);
            }
        }
        char[] answerArr = answer.toCharArray();
        for (char c : answerArr) {
            System.out.println(c);

        }
    }
}
