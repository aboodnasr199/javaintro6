package Mock2_JavaPractices;

import utilities.RandomGenerator;
import java.util.Arrays;
import java.util.Collections;

public class Practice05_Task03 {
    public static void main(String[] args) {
        withoutSort();
        withSort();
    }
    public static void withoutSort(){
        int[] arr = new int[5];

        for (int i = 0; i < 5; i++) {
            int random = RandomGenerator.getRandomNumber(1, 10);
            arr[i] = random;

        }
        System.out.println(Arrays.toString(arr));
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (int i : arr) {
            if (i > max){
                max = i;
            }
            if (i < min){
                min = i;
            }

        }
        System.out.println(max);
        System.out.println(min);


    }
    public static void withSort(){
        int[] arr = new int[5];
        Arrays.sort(arr);

        for (int i = 0; i < 5; i++) {
            int random = RandomGenerator.getRandomNumber(1, 10);
            arr[i] = random;

        }
        System.out.println(Arrays.toString(arr));
        System.out.println(arr[0]);
        System.out.println(arr[arr.length-1]);
    }
}
