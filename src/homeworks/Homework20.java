package homeworks;

public class Homework20 {
    public static void main(String[] args) {
        System.out.println("\n----------Task1--------\n");
        System.out.println(sum(new int[]{1, 5, 10}, true));
        System.out.println(sum(new int[]{3, 7, 10}, true));


        System.out.println("\n----------Task2--------\n");
        System.out.println(sumDigitsDouble("Java"));
        System.out.println(sumDigitsDouble("23abc45"));


        System.out.println("\n----------Task3--------\n");
        System.out.println(countOccurrence("Hello", "World"));
        System.out.println(countOccurrence("Can I can a can", "anc"));


    }


    public static int sum(int[] arr, boolean isEven) {
        int count = 0;

        for (int num : arr) {
            if (isEven) {
                if (num % 2 == 0) {
                    count++;
                }
            } else {
                if (num % 2 != 0) {
                    count++;
                }
            }
        }

        return count;
    }

    public static int sumDigitsDouble(String str) {
        int sum = 0;
        boolean hasDigits = false;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);
                sum += digit;
                hasDigits = true;
            } else if (c == '-') {
                int nextIndex = i + 1;
                if (nextIndex < str.length() && Character.isDigit(str.charAt(nextIndex))) {
                    i++;
                }
            }
        }

        return hasDigits ? sum * 2 : -1;
    }

    public static int countOccurrence(String first, String second) {
        String firstCleaned = first.replaceAll("\\s", "").toLowerCase();
        String secondCleaned = second.replaceAll("\\s", "").toLowerCase();

        int count = 0;
        int index = 0;

        while (index != -1) {
            index = secondCleaned.indexOf(firstCleaned, index);
            if (index != -1) {
                count++;
                index++;
            }
        }

        return count;
    }


}
