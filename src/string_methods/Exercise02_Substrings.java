package string_methods;

public class Exercise02_Substrings {
    public static void main(String[] args) {

        String str = "The best is Java";

        System.out.println(str.substring(12));

        // one way

        String sent = "I am learning java and I enjoy it";

        String java = sent.substring(12);

        //second way

        java = sent.substring(sent.lastIndexOf(' ')).trim();
        System.out.println(java);




    }
}
