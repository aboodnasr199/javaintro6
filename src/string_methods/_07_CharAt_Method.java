package string_methods;

import utilities.ScannerHelper;

import java.util.Scanner;

public class _07_CharAt_Method {
    public static void main(String[] args) {

        String str = "TechGlobal"; // last index is 9 | Length is 10
        String str2 = "Hello World"; // last index is 10 | Length is 11
        String str3 = "I really love java"; // last index is 17 | Length is 18

        System.out.println(str.charAt(4)); // G

        // last character way 1
        System.out.println(str.charAt(9)); // l

        //another way to get last character
        System.out.println(str.charAt(str.length()-1));
        System.out.println(str2.charAt(str2.length()-1));
        System.out.println(str3.charAt(str3.length()-1));

        System.out.println("\n----task----\n");

       String str4 = ScannerHelper.getString().trim();

        System.out.println(str4.charAt(str4.length() - 1));





    }
}
