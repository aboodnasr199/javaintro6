package casting;

public class Excerise02 {
    public static void main(String[] args) {
        /* Phone -> $900
        $50 per day

        how many days later, you can buy this phone

         */

        double price = 900;
        double dailySaveAmount = 50;

        System.out.println( "You can buy the phone after " + (int) (price / dailySaveAmount) + " days.");

    }
}
