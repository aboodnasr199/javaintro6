package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("\n------task1-----\n");

        String name = ("Adam");
        System.out.println("My name is = " + name);

        System.out.println("\n------task2-----\n");

        char nameCharacter1 = 'A';
        char nameCharacter2 = 'd';
        char nameCharacter3 = 'a';
        char nameCharacter4 = 'm';

        System.out.println("The first letter of my name is = " + nameCharacter1);
        System.out.println("The second letter of my name is = " + nameCharacter2);
        System.out.println("The third letter of my name is = " + nameCharacter3);
        System.out.println("The fourth letter of my name is = " + nameCharacter4);

        System.out.println("\n------task3-----\n");

        String myFavMovie = "Scream", myFavSong = "Dreams", myFavCity = "Chicago",
                myFavActivity = "Soccer", myFavSnack = "Snickers";

        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite song is = " + myFavSong);
        System.out.println("My favorite city is = " + myFavCity);
        System.out.println("My favorite activity is = " + myFavActivity);
        System.out.println("My favorite snack is = " + myFavSnack);

        System.out.println("\n------task4-----\n");

        int myFavNumber = 7, numbersOfStatesIVisited = 5,
                numberOfCountriesIVisited = 8;

        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("The number of states I visited is = " + numbersOfStatesIVisited);
        System.out.println("The number of countries I visited is = " + numberOfCountriesIVisited);

        System.out.println("\n------task5-----\n");

        boolean amIAtSchoolToday = false;

        System.out.println("I am at school today = " + amIAtSchoolToday);

        System.out.println("\n------task6-----\n");

        boolean isWeatherNiceToday = false;

        System.out.println("Weather is nice today = " + isWeatherNiceToday);


    }
}
