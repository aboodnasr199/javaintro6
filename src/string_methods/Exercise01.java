package string_methods;



import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 2 strings");
        String str1 = input.nextLine();
        String str2 = input.nextLine();

        System.out.println(str1 + " " +  str2);

        System.out.println(str1.equals(str2));


    }
}
