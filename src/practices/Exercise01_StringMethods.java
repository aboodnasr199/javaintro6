package practices;

import utilities.ScannerHelper;

public class Exercise01_StringMethods {
    public static void main(String[] args) {

        System.out.println("\n------Task1-----\n");

        String str = ScannerHelper.getString();

        System.out.println("The string given is = " + str);


        System.out.println("\n------Task2-----\n");
        // way 1
        if (str.isEmpty()) {
            System.out.println("The given string is empty = " + str.isEmpty());
        } else {
            System.out.println("The length of the string is = " + str.length());
        }


        System.out.println("\n------Task3-----\n");

        if (str.isEmpty()) System.out.println("There is no character in this string");
        else {
            System.out.println("The first character = " + str.charAt(0));
        }

        System.out.println("\n------Task4-----\n");

        if (!str.isEmpty()) System.out.println("The last character is = " + str.charAt(str.length()- 1));
        else {
            System.out.println("There is no character in this string");
    }

        System.out.println("\n------Task5-----\n");

        // str = str.toUpperCase(); you use this if u dont wanna repeat the process of toUpperCase()
        if (str.toUpperCase().contains("A") || str.toUpperCase().contains("E") || str.toUpperCase().contains("I") ||
                str.toUpperCase().contains("U") || str.toUpperCase().contains("O"))
            System.out.println("This String has a vowel");
        else {
            System.out.println("This string does not have a vowel");
        }

    }

}

