package singleton;

public class Phone {
    private static Phone phone; // singleton.phone
    private Phone(){
        //default constructor
    }

    public static Phone getPhone() {
        if(phone == null) phone = new Phone();
        return phone;
    }

    public static void setPhone(Phone phone) {
        Phone.phone = phone;
    }
}
