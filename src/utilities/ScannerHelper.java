package utilities;

import java.util.Scanner;

public class ScannerHelper {
    static Scanner input = new Scanner(System.in);
    // Write a method that ask and return a name from user

    public static String getFirstName() {
        System.out.println("Please enter a first name:");
        return input.nextLine();
    }


        public static String getLastName(){
            System.out.println("Please enter a last name:");
            return input.nextLine();
        }
        public static int getAge(){
            System.out.println("Please enter an age:");
            int age = input.nextInt();
            input.nextLine();

            return age;
        }
        public static int getNumber(){
            System.out.println("Please enter a number:");
            int number = input.nextInt();
            input.nextLine();

            return number;
        }
        public static String getString(){
            System.out.println("Please enter a string");
            String str = input.nextLine();

            return str;
        }
        public static String getAddress(){
            System.out.println("Please enter address");
            String str1 = input.nextLine();

            return str1;
        }
        public static String getFavCountry(){
            System.out.println("Please enter your favortie country");
            String str2 = input.nextLine();

            return str2;
        }
    }

