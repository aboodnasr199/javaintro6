package string_methods;

import java.util.Scanner;

public class Exercise05 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a word");
        String str = input.nextLine();

        boolean startsWith = str.startsWith("a") && (str.endsWith("e"));

        System.out.println(startsWith);
    }
}
