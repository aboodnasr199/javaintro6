package Mock2_JavaPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class Practice06_Task05 {
    public static void main(String[] args) {
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(200, 5, 100, 99, 101, 75))));

    }
    public static String remove3orMore(ArrayList<Integer> list){

        for (int i = 0; i < list.size(); i++) {
            list.removeIf(element -> element >= 100 || element <= -100);

        }
        return list.toString();
    }
}
