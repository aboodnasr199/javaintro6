package enums;

public class Constants {

    // we can create multiple enums inside a class

    public enum Gender{
        FEMALE,
        MALE,
        OTHER
    }

    public enum TShirtSize{
        X_SMALL,
        SMALL,
        MEDIUM,
        LARGE,
        X_LARGE
    }
    public enum Direction{
        NORTH,
        SOUTH,
        WEST,
        EAST
    }
}
