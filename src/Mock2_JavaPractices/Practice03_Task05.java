package Mock2_JavaPractices;

import utilities.ScannerHelper;

public class Practice03_Task05 {
    public static void main(String[] args) {
            String str1 = ScannerHelper.getString();
            String str2 = ScannerHelper.getString();

            if (str1.length() > 2 || str2.length() > 2) {
                System.out.println(str1.substring(1, str1.length()-1) + str2.substring(1, str2.length()-1));
            }
            else{
                System.out.println("Invalid Input");
            }
        }
    }

