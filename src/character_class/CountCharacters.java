package character_class;

import utilities.ScannerHelper;

public class CountCharacters {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        int digit = 0;
        int letters = 0;

        for (int i = 0; i <= str.length()-1; i++) {
            if (Character.isDigit(str.charAt(i))) digit++;
            else if (Character.isLetter(str.charAt(i))) letters++;
        }
        System.out.println("This string has " + digit + " digits and " + letters + " letters");

        System.out.println("\n----Task2----\n");

        str = ScannerHelper.getString();
        int upper = 0;
        int lower = 0;
        for (int i = 0; i <= str.length()-1 ; i++) {
            if (Character.isUpperCase(str.charAt(i))) upper++;
            else if (Character.isLowerCase(str.charAt(i))) lower++;

        }
        System.out.println("This string has " + upper + " uppercase letters and " + lower + " lowercase letters");

        System.out.println("\n----Task3----\n");

        str = ScannerHelper.getString();

        int special = 0;

        for (int i = 0; i < str.length(); i++) {
            if (!Character.isLetterOrDigit(str.charAt(i)) && !Character.isWhitespace(str.charAt(i))) special++;
        }
        System.out.println("This string has " + special + " special characters");

        System.out.println("\n----Task4----\n");

        str = ScannerHelper.getString();

        int letter = 0;
        int uppers = 0;
        int lowers = 0;
        int digits = 0;
        int spaces = 0;
        int specials = 0;

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isLetter(c)){
                letter++;
                if (Character.isUpperCase(c)) uppers++;
                else lowers++;

            }
            else if (Character.isDigit(c)) digits++;
            else if (Character.isWhitespace(c)) spaces++;
            else specials++;


        }
        System.out.println("letters = " + letter);
        System.out.println("Uppercase = " + uppers);
        System.out.println("Lowercase = " + lowers);
        System.out.println("Digits = " + digits);
        System.out.println("Spaces = " + spaces);
        System.out.println("Specials = " + specials);

    }
}
