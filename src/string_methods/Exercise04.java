package string_methods;


public class Exercise04 {
    public static void main(String[] args) {

        /*
        1. return
        2. returning a string
        3. nonstatic
        4. it has arguments, ints.
         */
        String str = "I go to TechGlobal";

        String firstWord = str.substring(0, 1);
        String secondWord = str.substring(2, 4);
        String thirdWord = str.substring(5, 7);
        String fourthWord = str.substring(8, 18);

        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);
        System.out.println(fourthWord);



    }
}
