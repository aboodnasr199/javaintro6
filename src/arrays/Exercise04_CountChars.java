package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {
        /*
        write a program that ask user to enter a string

        count how many characters in the string are letters
         */
        String str = ScannerHelper.getString();

        System.out.println("\n-------1st way------\n");


        int letters = 0;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isLetter(str.charAt(i))) letters++;

        }
        System.out.println(letters);

        System.out.println("\n-------2nd way-----\n");

        str = ScannerHelper.getString();
        int count = 0;
        char[] chars = str.toCharArray();

        for (char c : chars) {
            if (Character.isLetter(c)) count++;

        }
        System.out.println(count);
    }
}
