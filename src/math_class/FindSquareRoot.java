package math_class;

public class FindSquareRoot {
    public static void main(String[] args) {
        // 5^2 =25
        // square root of 25 == 5

        System.out.println(Math.sqrt(25));
        System.out.println(Math.sqrt(30-5.0));
        System.out.println(Math.sqrt(5));
        System.out.println(Math.sqrt(-5.76262672));
        System.out.println(Math.sqrt(-25.0));


        System.out.println(Math.round(5.5)); // 5
        System.out.println(Math.round(45.3)); // 45
        System.out.println(Math.round(1.499999));// 1


    }
}
