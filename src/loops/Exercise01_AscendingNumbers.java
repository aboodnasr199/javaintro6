package loops;

public class Exercise01_AscendingNumbers {
    public static void main(String[] args) {
        // write a code that print 1-10, 10 included.
        for (int i = 1; i <= 10; i++){
            System.out.println(i);
        }
    }
}
