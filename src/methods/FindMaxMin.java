package methods;

import utilities.MathHelper;
import utilities.ScannerHelper;

public class FindMaxMin {
    public static void main(String[] args) {
        int n1 = ScannerHelper.getNumber();
        int n2 = ScannerHelper.getNumber();
        int n3 = ScannerHelper.getNumber();

        System.out.println("The max = " + MathHelper.findMax(n1, n2, n3));

        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        System.out.println("The sum of the 2 numbers is = " + MathHelper.sum(num1, num2));

        int number1 = ScannerHelper.getNumber();
        int number2 = ScannerHelper.getNumber();

        System.out.println("The product of the 2 numbers is = " + MathHelper.product(number1, number2));

        int a = ScannerHelper.getNumber();

        System.out.println("The square root of the number is = " + MathHelper.square(a));

    }
}
