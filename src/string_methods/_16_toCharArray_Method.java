package string_methods;

import java.util.Arrays;

public class _16_toCharArray_Method {
    public static void main(String[] args) {

        String name = "John";

        char[] charsOfName = name.toCharArray();

        System.out.println(Arrays.toString(charsOfName));

        // print the element at the index of 1 -> o
        System.out.println(charsOfName[1]);
        // length of string
        System.out.println(charsOfName.length);
        // iterate the string
        for (char c : charsOfName) {
            System.out.println(c);
        }

    }
}
