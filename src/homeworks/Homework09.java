package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework09 {
    public static void main(String[] args) {
        System.out.println("\n--------Task1-------\n");
        System.out.println(firstDuplicated(new int[]{-4, 0, -7, 0, 5, 10, 45, 45}));
        System.out.println(firstDuplicated(new int[]{-8, 56, 7, 8, 65}));


        System.out.println("\n--------Task2-------\n");
        System.out.println(firstDuplicatedString(new String[]{"Z", "abc", "z", "123", "#"}));
        System.out.println(firstDuplicatedString(new String[]{"xyz", "java", "abc", "123", "#"}));

        System.out.println("\n--------Task3-------\n");
        System.out.println(duplicatedNumbers(new int[]{0, -4, -7, 0, 5, 10, 45, -7, 0}));
        System.out.println(duplicatedNumbers(new int[]{1, 2, 5, 0, 7}));

        System.out.println("\n--------Task4-------\n");
        System.out.println(duplicatedStrings(new String[]{"A", "foo", "12", "Foo", "bar", "a", "java"}));
        System.out.println(duplicatedStrings(new String[]{"python", "foo", "bar", "java", "123"}));

        System.out.println("\n--------Task5-------\n");
        reverseArray(new String[]{"abc", "foo", "bar"});
        reverseArray(new String[]{"ruby", "python", "java"});

        System.out.println("\n--------Task6-------\n");
        String str = "Java is fun";
        reverseStringWords(str);




    }

    public static int firstDuplicated(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int current = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                if (current == arr[j]) {
                    return current;
                }

            }
        }
        return -1;
    }
    public static String firstDuplicatedString(String[] arr){

        for (int i = 0; i < arr.length; i++) {
            String current = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                if (current.equalsIgnoreCase(arr[j])){
                    return current;
                }
            }

        }
        return "There is no duplicates";
    }
    public static ArrayList<Integer> duplicatedNumbers(int[] arr){
        ArrayList<Integer> duplicates = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            int current = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                if (current == arr[j] && !duplicates.contains(current)){
                    duplicates.add(current);
                }

            }

        }
        return duplicates;
    }
    public static ArrayList<String> duplicatedStrings(String[] arr){
        ArrayList<String> duplicates = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            String current = arr[i];
            for (int j = i+1; j < arr.length; j++) {
                if (current.equalsIgnoreCase(arr[j]) && !duplicates.contains(current))
                    duplicates.add(current);

            }

        }
        return duplicates;
    }
    public static void reverseArray(String[] arr){
        String[] reverse = new String[arr.length];

        for (int i = arr.length-1; i >= 0; i--) {
            reverse[i] = arr[i];

        }
        System.out.println(Arrays.toString(reverse));
    }
    public static void reverseStringWords(String str){
        String[] words = str.split("\\s+");
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            StringBuilder reversedWord = new StringBuilder(words[i]).reverse();
            result.append(reversedWord);
            if (i < words.length - 1) {
                result.append(" ");
            }
        }
        System.out.println(result.toString());

    }
}

