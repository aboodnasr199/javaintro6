package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise01_CountStrings {
    public static void main(String[] args) {
        System.out.println(countO(new ArrayList<>(Arrays.asList("Hello", "Hi", "School", "Computer"))));
        System.out.println(countO(new ArrayList<>(Arrays.asList("abc", "xyz"))));
        System.out.println(countO(new ArrayList<>(Arrays.asList())));
        System.out.println(countO(new ArrayList<>(Arrays.asList("Object", "Laptop"))));

        System.out.println(more3(new ArrayList<>(Arrays.asList("Hello", "Hi", "School", "Computer"))));
        System.out.println(more3(new ArrayList<>(Arrays.asList("abc", "xyz"))));
        System.out.println(more3(new ArrayList<>(Arrays.asList())));
        System.out.println(more3(new ArrayList<>(Arrays.asList("Object", "Laptop"))));

    }
    public static int countO(ArrayList<String> list){
        int o = 0;
        for (String s : list) {
            if (s.toLowerCase().contains("o")) o++;
        }
        return o;
    }


    public static int more3(ArrayList<String> str){
        int letters = 0;
        for (String s : str) {
            if (s.length() >= 3) letters++;

        }
        return letters;
    }
}
