package Mock2_JavaPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class Practice06_Task02 {
    public static void main(String[] args) {
        System.out.println(secondMax(new ArrayList<>(Arrays.asList(2,5,2,1,1,7,1))));
        System.out.println(secondMax(new ArrayList<>(Arrays.asList(5,7,2,2,10,10))));

    }
    public static int secondMax(ArrayList<Integer> list){
         int secondMax = 0;
         int max = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > max){
                secondMax = max;
                max = list.get(i);
            }
        }
        return secondMax;
    }
}
