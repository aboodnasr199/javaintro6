package recursion;

public class CodingWithJohnRecursion  {

    /*
    Recursion - When you have a method that calls itself
    How to use -
    Structures -
    base case- a condition inside your method to stop infinite recursion- line 17-20
     */
    public static void main(String[] args) {
        sayHi(3);
    }

    private static void sayHi(int count){
        System.out.println("Hi!");
        if (count <= 1){
            return;
        }
        sayHi(count-1);
    }
}
