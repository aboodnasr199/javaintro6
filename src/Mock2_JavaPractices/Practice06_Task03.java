package Mock2_JavaPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class Practice06_Task03 {
    public static void main(String[] args) {
        System.out.println(secondMin(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1))));

    }
    public static int secondMin(ArrayList<Integer> list){

        int min = Integer.MAX_VALUE;
        int secondMin = Integer.MAX_VALUE;

        int max = Integer.MIN_VALUE;
        int secondMax = Integer.MIN_VALUE;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) < min){
                secondMin = min;
                min = list.get(i);
            }
            if (list.get(i) > max){
                secondMax = max;
                max = list.get(i);
            }

        }
        System.out.println(secondMin);
        System.out.println(secondMax);
        return 0;

    }
}
