package loops.practices;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {
        int num = ScannerHelper.getNumber();
        while (!(num >= 10)){
            num = ScannerHelper.getNumber();
            if (num < 10) System.out.println("This number is less than 10");
        }
        System.out.println("The number is more or equal to 10");

        System.out.println("\n======do while loop====\n");
        int num1;
        do {
            num1 = ScannerHelper.getNumber();
            if (num1 < 10) System.out.println("This number is less than 10");
        }while (num1 < 10);
        System.out.println("The number is more or equal to 10");

    }
}
