package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise02 {
    public static void main(String[] args) {
        String input = ScannerHelper.getString();


        Pattern regex = Pattern.compile("[a-zA-z]{1,}");
        Matcher matcher = regex.matcher("Hello, my name is John Doe");
        int counter = 0;
        while (matcher.find()){
            counter++;
            System.out.println(matcher.group());
        }
        System.out.println("Word count is = " + counter);

    }
}
