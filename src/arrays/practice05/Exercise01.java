package arrays.practice05;

public class Exercise01 {
    public static void main(String[] args) {
        getFirstPosAndNeg();

        int[] numbers = {0, -4, -7, 0, 5, 10, 45};

        boolean isPositive = false;
        boolean isNegative = false;

        for (int number : numbers) {
            if (!isPositive && number > 0){
                System.out.println(number);
                isPositive = true;
            }
           else if (!isNegative && number < 0){
                System.out.println(number);
                isNegative = true;
            }
           if (isPositive && isNegative)
               break;
        }
    }

    public static void getFirstPosAndNeg(){

        int[] arr = {0, -4, -7, 0, 5, 10, 45};
        int firstPos = 0;
        int firstNeg = 0;

        for (int n : arr) {
            if (n > 0) {
                firstPos = n;
                break;
            }
        }
        for (int n : arr) {
            if (n < 0){
                firstNeg = n;
                break;
            }

        }
        System.out.println("First postive number is = " + firstPos );
        System.out.println("First negative number is = " + firstNeg );

    }
}