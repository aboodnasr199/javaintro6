package discord_practices;

public class CodingBat02 {
    public static void main(String[] args) {
        /*
        Given an array of ints,
        return true if the array is length 1 or more
        ,and the first element and the last element are equal.
         */
        int[] arr = {0,0,2,3,4,0,0};
        firstAndLast(arr);
    }
    public static boolean firstAndLast(int[] equal) {
        if (equal.length >= 1 && equal[0] == equal[equal.length - 1]) {
            System.out.println(true);
        }
       else {
            System.out.println(false);
        }
        return false;
    }

}
