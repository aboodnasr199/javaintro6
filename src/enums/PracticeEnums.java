package enums;

import java.time.Month;

public class PracticeEnums {
    public static void main(String[] args) {

        System.out.println(DaysOfTheWeek.Sunday);

        for (DaysOfTheWeek days : DaysOfTheWeek.values()) {
            System.out.println(days);

        }
        for (Month m : Month.values()){
            System.out.println(m);
    }
        System.out.println(Constants.TShirtSize.MEDIUM);
        System.out.println(Constants.Direction.WEST);

        for (Constants.Gender value : Constants.Gender.values()) {
            System.out.println(value);

        }
    }


}
