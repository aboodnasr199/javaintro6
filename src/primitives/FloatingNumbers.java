package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {

        /*
            Floating numbers: 10.5, 23.32423, 10000.234135

            float --> 4 bytes
            double --> 8 Bytes
         */
        System.out.println("\n-----floating numbers----\n");
        float myFloat1 = 20.5F;
        double myDouble1= 20.5;

        System.out.println(myFloat1);
        System.out.println(myDouble1);

        System.out.println("\n-----floating numbers----\n");
        float myfloat2 = 10;
        double myDouble2 = 234235;

        System.out.println(myfloat2); // 10.0
        System.out.println(myDouble2); // 234235.0

        float f1 = 32746237.23423542352352F;
        double d1 = 32746237.23423542352352;

        System.out.println(f1);
        System.out.println(d1);

    }
}
