package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
        //VARIABLE DECLERATION

        int age; // declaring the variable without a value

        age = 25; // initializing the variable

        System.out.println(age = 25);

        String name = "Adam"; // declaring and initializing the variable


        // capital letters and lowercase letter are seen differently with java
        double money = 5.5;

        double d1 = 10;

        System.out.println(d1);

        d1 = 15.5;
        System.out.println(d1);


        }



    }

