package primitives;

public class AbsoluteNumbers {
    public static void main(String[] args) {
        /*
            Absloute numbers: 1, 5, -3, -125, 2341243534
            Byte ->
            short
            int
            long
            dataType variableName = Value;
         */

        System.out.println("\n----byte----\n");
        byte myNumber = 45;
        System.out.println(myNumber); // 45

        System.out.println("the max value of byte = " + Byte.MAX_VALUE);
        System.out.println("the max value of byte = " + Byte.MIN_VALUE);

        System.out.println("\n----short----\n");
        short numberShort = 150;

        System.out.println(numberShort);

        System.out.println("The max value of short = " + Short.MAX_VALUE); //32767
        System.out.println("The min value of short = " + Short.MIN_VALUE); //-32768

        System.out.println("\n----Int----\n");

        int myInteger= 20000000;
        System.out.println(myInteger);
        System.out.println("The max value of integer = " + Integer.MAX_VALUE);
        System.out.println("The max value of integer = " + Integer.MIN_VALUE);

        System.out.println("\n----long----\n");
        long l1 = 5;
        long l2 = 2387533681L;

        System.out.println(l1);




    }
}
