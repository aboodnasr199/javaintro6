package Mock2_JavaPractices;

import utilities.ScannerHelper;

import java.util.ArrayList;

public class Practice01 {
    public static void main(String[] args) {
    String word = ScannerHelper.getString();
    task1(word);
    }
    public static void task1(String word){


        if(word.length() >= 1) {
            System.out.println("Length is = " + word.length());
            System.out.println("First char is = " + word.charAt(0));
            System.out.println("Last char is = " + word.charAt(word.length() - 1));


            if (word.toLowerCase().contains("a") || word.toLowerCase().contains("e") || word.toLowerCase().contains("i")
            || word.toLowerCase().contains("o") || word.toLowerCase().contains("u")){
                System.out.println("This string has vowles");
            }

            else{
                System.out.println("This string has no vowels ");
            }

        }
        else {
            System.out.println("Length is 0");
        }
        }
    }


