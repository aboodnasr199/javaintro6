package strings;

public class Exercise02 {
    public static void main(String[] args) {

        System.out.println("\n------exercise1----\n");
        String firstName;
        String lastName;
        String dateOfBirth;
        String address;

        firstName = "Adam";
        lastName = "Nasr";
        dateOfBirth = "January 1st";
        address = "12345 mary ln";

        System.out.println(firstName);
        System.out.println(lastName);
        System.out.println(dateOfBirth);
        System.out.println(address);

        System.out.println("\n------exercise2----\n");

        String wordPart1 = "le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";

        String wordPart4 = wordPart1 + wordPart2 + wordPart3;

        System.out.println(wordPart4);


    }
}
