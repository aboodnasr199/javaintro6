package loops;

public class UnderstandingLoops {
    public static void main(String[] args) {
        /*
        for loop syntax

        for(initialization (startpoint); termination; update){
            // block of code to be executed
            }

            intialization -> start point
            termination -> stop condition
            update -> increased or decreasing -> increment or decrement operators
         */
        //print hello 5 times or 20 times

        for (int num = 0; num < 5; num++){
            System.out.println("Hello World!");

        }

    }
}
