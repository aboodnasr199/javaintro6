package enums;

public class Practice02 {
    public static void main(String[] args) {

        Frequency money = Frequency.HOURLY;

        switch (money){
            case HOURLY:
                System.out.println("Hourly -> $20");
                break;
            case DAILY:
                System.out.println("Daily -> $160");
                break;
            case WEEKLY:
                System.out.println("Weekly -> $800");
                break;
            case BI_WEEKLY:
                System.out.println("Bi_Weekly -> $1600");
                break;
            case MONTHLY:
                System.out.println("Monthly -> $3200");
                break;
            case YEARLY:
                System.out.println("Yearly -> $38400");
                break;
            default:
                throw new IllegalStateException("This value is not an enum");
        }
    }
}
