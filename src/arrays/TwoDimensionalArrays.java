package arrays;

import java.util.Arrays;

public class TwoDimensionalArrays {
    public static void main(String[] args) {
        String[][] students = {
                {"Meerim", "Alina", "Carmela", "Ayat"},
                {"Yahya", "Adam", "Louie"},
                {"Dima", "Lesia", "Pinar"}
        };

        //how to print 2-dimentional array
        System.out.println(Arrays.deepToString(students));

        // how to print an inner array
        System.out.println(Arrays.toString(students[0]));
        System.out.println(Arrays.toString(students[1]));
        System.out.println(Arrays.toString(students[2]));

        //how to print an element from an inner array
        System.out.println(students[1][2]);

        //how to print length of each inner array
        System.out.println(students[0].length);
        System.out.println(students[1].length);
        System.out.println(students[2].length);

        System.out.println("\n------loop 2-dimensional array-----\n");
        //how to loop 2-dimensional array
        for (int i = 0; i < students.length; i++) {
            System.out.println(Arrays.toString(students[i]));

        }
        for (String[] innerArray : students) {
            System.out.println(Arrays.toString(innerArray));
        }
        System.out.println("\n------loop each element-----\n");
        //how to loop 2 dimensional for each element
        for (String[] inner : students) {
            //each inner array is here
            for (String name : inner) {
                //each name is here
                System.out.println(name);

            }


        }
        // Create a container that will store 5 groups of 3 numbers

        int[][] numbers = new int[5][3];
        System.out.println(Arrays.deepToString(numbers));
        numbers[3][1] = 9;
        numbers[2][0] = 5;
        numbers[0][0] = 7;
        System.out.println(Arrays.deepToString(numbers));

        }


    }

