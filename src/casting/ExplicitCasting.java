package casting;

public class ExplicitCasting {
    public static void main(String[] args) {
        /*
        Explicit casting is storing bigger data types into smaller data types
        it does not happen automatically and programmer have to resolve the compiler issue
        Ex.
        long -> byte
        double -> float
        double -> int
        int -> short
         */
        long number1 = 200;

        byte number2 = (byte) number1;

        System.out.println(number2);
    }
}
