package operators.increment_decrement_operators;


import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a number");


        int number = input.nextInt();
        int index = 1;
        int index2 = -1;

        System.out.println(number * index++);
        System.out.println(number * index++);
        System.out.println(number * index++);
        System.out.println(number * index++);
        System.out.println(number * index++);

        System.out.println(number * index2--);
        System.out.println(number * index2--);
        System.out.println(number * index2--);
        System.out.println(number * index2--);
        System.out.println(number * index2--);



    }
}
