package arrays;

import java.util.Arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {

        int[] numbers = {3, 10, 8, 5, 5};

        System.out.println("\n====LOOP WAY=====\n");
       boolean has5 = false;

        for (int number : numbers) {
            if (number == 5){
                has5 = true;
                break;
            }
        }
        System.out.println(has5);


        System.out.println("\n=====BINARY SEARCH WAY======\n");
        // binary search cannot be used without sorting
        // if it does not exist it will always return negative
        Arrays.sort(numbers);
        System.out.println(Arrays.binarySearch(numbers, 5)); // returns the index of 5 if found - > 2
        System.out.println(Arrays.binarySearch(numbers, 10)); // returns index 4
        System.out.println(Arrays.binarySearch(numbers, 3)); // 0
        System.out.println(Arrays.binarySearch(numbers, 7)); // -4 > -3-1 = -4
        System.out.println(Arrays.binarySearch(numbers,15)); // -6 -> -5-1= -6
        System.out.println(Arrays.binarySearch(numbers, 1)); // -1




    }
}
