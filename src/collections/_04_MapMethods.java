package collections;

import java.util.HashMap;
import java.util.Map;

public class _04_MapMethods {
    public static void main(String[] args) {

        HashMap<String, String> capitals = new HashMap<>();

        System.out.println("\n-------How to add entries to a map------\n");
        capitals.put("France", "Paris");
        capitals.put("Italy", "Rome");
        capitals.put("Spain", "Madrid");


        System.out.println("\n ------- how to print out map --------\n");
        System.out.println(capitals);


        System.out.println("\n------How to retrieve a value------\n");
        System.out.println(capitals.get("France")); // paris
        System.out.println(capitals.get("Italy")); // rome
        System.out.println(capitals.get("Spain")); // Madrid
        System.out.println(capitals.get("USA")); // Null


        System.out.println("\n--------how to check if map contains given key or value---------\n");
        System.out.println(capitals.containsKey("Italy"));
        System.out.println(capitals.containsKey("Belgium"));


        System.out.println(capitals.containsKey("Rome"));
        System.out.println(capitals.containsKey("madrid"));


        System.out.println("\n-------How to remove some entries-----\n");
        System.out.println(capitals.remove("USA"));
        System.out.println(capitals.remove("Italy", "rome"));

        System.out.println(capitals.remove("Italy"));
        System.out.println(capitals.remove("Spain", "Madrid"));
        System.out.println(capitals);

        System.out.println("\n-------How to get size of the map------\n");
        System.out.println(capitals.size());

        capitals.put("Ukraine", "Kyiv");
        capitals.put("Turkiye", "Ankara");

        System.out.println(capitals);
        System.out.println(capitals.size());

        System.out.println("\n-----How to get all keys-------\n");
        System.out.println(capitals.keySet());

        for (String key: capitals.keySet()) {
            System.out.println(key);
        }


        System.out.println("\n-------How to get all values-----\n");
        System.out.println(capitals.values());


        System.out.println("\n---------How to get all entries--------\n");
        System.out.println(capitals.entrySet());


    }
}
