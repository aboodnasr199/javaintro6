package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework10 {
    public static void main(String[] args) {
        System.out.println("\n--------Task1-------\n");
        System.out.println(countWord("   Java is fun  "));
        System.out.println(countWord("Selenium is the most common UI automation tool.  "));

        System.out.println("\n--------Task2-------\n");
        System.out.println(countA("TechGlobal is a QA bootcamp"));
        System.out.println(countA("QA stands for Quality Assurance"));

        System.out.println("\n--------Task3-------\n");
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67))));
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123))));

        System.out.println("\n--------Task4-------\n");
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3))));

        System.out.println("\n--------Task5-------\n");
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"))));

        System.out.println("\n--------Task6-------\n");
        System.out.println(removeExtraSpaces("     I  am   learning  Java  "));
        System.out.println(removeExtraSpaces("Java  is fun  "));

        System.out.println("\n--------Task7-------\n");
        System.out.println(Arrays.toString(adds(new int[]{10, 3, 6, 3, 2}, new int[]{6, 8, 3, 0, 0, 7, 5, 10, 34})));
        System.out.println(Arrays.toString(adds(new int[]{3, 0, 0, 7, 5, 10}, new int[]{6, 3, 2})));

        System.out.println("\n--------Task8-------\n");
        System.out.println(findClosestTo10(new int[]{10, -13, 5, 70, 15, 57}));
        System.out.println(findClosestTo10(new int[]{10, -13, 3, 4, 15, 10}));


    }

    public static int countWord(String str) {

        return str.trim().split(" ").length;

    }

    public static int countA(String str) {
        int c = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.toLowerCase().charAt(i) == 'a') {
                c++;
            }

        }
        return c;
    }

    public static int countPos(ArrayList<Integer> arr) {

        int count = 0;

        for (Integer i : arr) {
            if (i > 0) {
                count++;
            }

        }
        return count;
    }

    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> arr) {

        ArrayList<Integer> newList = new ArrayList<>();

        for (int result : arr) {
            if (!newList.contains(result)) newList.add(result);

        }
        return newList;
    }

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> arr) {

        ArrayList<String> newList = new ArrayList<>();

        for (String s : arr) {
            if (!newList.contains(s)) newList.add(s);

        }
        return newList;
    }

    public static String removeExtraSpaces(String str) {

        return str.replaceAll("\\s+", " ").trim();
    }

    public static int[] adds(int[] arr1, int[] arr2) {

        int shortest = Math.min(arr1.length, arr2.length);
        int longest = Math.max(arr1.length, arr2.length);

        int[] bigger;

        if (arr1.length > arr2.length){
            bigger = arr1;
        }else bigger = arr2;



        int[] sum = new int[longest];

        for (int i = 0; i < longest; i++) {
            if (i < shortest) {
                sum[i] = arr1[i] + arr2[i];
            } else {
                sum[i] = bigger[i];
            }

        }
        return sum;

    }

    public static int findClosestTo10(int[] arr) {

        int closest = Integer.MAX_VALUE;
        for (int current : arr) {
            if (current == 10) {
                continue;
            }
            if (Math.abs(current - 10) < Math.abs(closest - 10)) {
                closest = current;
            }
        }
        return closest;


    }
}
