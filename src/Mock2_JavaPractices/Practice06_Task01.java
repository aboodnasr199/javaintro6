package Mock2_JavaPractices;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Practice06_Task01 {
    public static void main(String[] args) {
        int [] result = double1(new int[]{3, 2, 5, 7, 0});
        System.out.println(Arrays.toString(result));

    }
    public static int[] double1(int[] list){

        for (int i = 0; i < list.length; i++) {
           list[i] = list[i] * 2;
        }
        return list;
    }
}
