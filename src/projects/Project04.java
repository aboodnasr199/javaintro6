package projects;

import utilities.ScannerHelper;

import java.util.Locale;

public class Project04 {
    public static void main(String[] args) {

        System.out.println("\n-------task1-----\n");

        String str = ScannerHelper.getString();

        if (str.length() >= 8)
            System.out.println(str.substring(str.length() - 4) + str.substring(4, str.length() - 4)
                    + str.substring(0, 4));
        else System.out.println("This String does not have 8 characters");


        System.out.println("\n-------task2-----\n");

        String str2 = ScannerHelper.getString();

        if (str2.contains(" ")) {
            System.out.println(str2.substring(str2.lastIndexOf(' ') + 1)
                    + str2.substring(str2.indexOf(' '), str2.lastIndexOf(' '))
                    + " " + str2.substring(0, str2.indexOf(' ')));
        } else {
            System.out.println("The sentence does not have two or more words to swap");

        }


        System.out.println("\n-------task3-----\n");

        String str3 = ScannerHelper.getString();

        System.out.println(str3.replaceAll("(stupid | idiot )", "nice"));

        System.out.println(str3.replace("stupid", "nice").replace("idiot", "nice"));


        System.out.println("\n-------task4-----\n");

        String str4 = ScannerHelper.getFirstName();

        if (str4.length() < 2) {
            System.out.println("Invalid input");
        } else if (str4.length() % 2 == 1) {
            System.out.println(str4.substring(2, str4.length() - 2));
        } else {
            System.out.println(str4.substring(str4.length() / 2 - 1, str4.length() / 2 + 1));
        }


        System.out.println("\n-------task5-----\n");

        String country = ScannerHelper.getFavCountry();

        if (country.length() >= 5) System.out.println(country.substring(2, country.length() - 2));
        else System.out.println("Invalid input");


        System.out.println("\n-------task6-----\n");

        String address = ScannerHelper.getAddress();

        System.out.println(address.replace('a', '*').replace('A', '*')
                .replace('e', '#').replace('E', '#')
                .replace('i', '+').replace('I', '+')
                .replace('u', '$').replace('U', '$')
                .replace('o', '@').replace('O', '@'));


    }
}
