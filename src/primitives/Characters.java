package primitives;

public class Characters {
    public static void main(String[] args) {
        /*
         Char -> 2 bytes
         it is used to store a single character
         letter, digit, space, specials
         
         dataType variable Name = 'Value'
         */

        char c1 = 'A';
        char c2 = ' ';

        System.out.println(c1);
        System.out.println(c2);

        char myFavCharacter = '7';

        System.out.println("My favorite char = " + myFavCharacter);
    }
}
