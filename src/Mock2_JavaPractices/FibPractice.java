package Mock2_JavaPractices;

import java.util.Scanner;

public class FibPractice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();

        int first = 0;
        int second = 1;


        // 0,1,1,2,3,5,8,13

        System.out.print(first + " - " + second);
        for (int i = 2; i < num; i++) {
            int nextNum = first + second;
            first = second;
            second = nextNum;

            System.out.print(" - " + nextNum);
        }

    }
}
