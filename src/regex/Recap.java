package regex;

public class Recap {
    public static void main(String[] args) {
        /*
        Meta Characters
        . -> Any character
        \\d -> [0-9] -> any digit
         \\D ->[^0-9] -> any character but not digit
          \\s -> space character
          \\S -> any character but not space


         */


        String str = "123 St Chicago IL 12345.";

        //special characters
        System.out.println(str.replaceAll("[0-9a-zA-Z ]", ""));
        System.out.println(str.replaceAll("[0-9a-zA-Z ]", "").length());




    }
}
