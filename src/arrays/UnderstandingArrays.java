package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {

        String[] cities = {"Chicago", "Miami", "Toronto", "Rome", "Berlin"};

        //The number of elements in array

        System.out.println(cities.length); // 3

        // Get paticular element from array

        System.out.println(cities[1]); // miami
        System.out.println(cities[0]); // chicago
        System.out.println(cities[2]); // tornoto
        System.out.println(cities[1] + cities[2]);

        //index out of bounds
        //System.out.println(cities[-1]);

        //How to print array with all elements?
        // 2 steps: 1. convert your array to a String
        // 2. print it with print method
        System.out.println(Arrays.toString(cities));


        System.out.println("\n=====For loop=====\n");
        // How to loop array
        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);

            System.out.println("\n== for loop - enhanced loop ===\n");
            for (String element : cities){
                System.out.println(element);
            }

        }
    }
}
