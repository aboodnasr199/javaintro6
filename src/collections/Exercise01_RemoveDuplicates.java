package collections;

import java.util.List;
import java.util.TreeSet;

public class Exercise01_RemoveDuplicates {

    public static void main(String[] args) {

    }

    /*
    if order matters to remove duplicates use tree set if not use hashset
     */

    public static TreeSet<Integer> removeDuplicates(List<Integer> nums){
        return new TreeSet<>(nums);
    }
}
