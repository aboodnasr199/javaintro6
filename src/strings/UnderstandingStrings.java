package strings;

public class UnderstandingStrings {
    public static void main(String[] args) {

        String s1; // declaration of s1 as a string

        s1 = "techGlobal School"; // intializing s1 as techGlobal School

        String s2 = "is the best"; // intializing and declaring s2 as is the best

        String s3 = s1 + " " + s2; // concatination using plus sign

        System.out.println(s3);

        String s4 = s1.concat(" ").concat(s2);
        System.out.println(s4);



        System.out.println(s3);



    }
}
