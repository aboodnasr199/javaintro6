package projects;

import java.util.ArrayList;
import java.util.Arrays;

public class Project07 {
    public static void main(String[] args) {
        System.out.println("\n-------Task01------\n");
        System.out.println(countMultipleWords(new String[]{"foo", "", "foo bar", "java is fun", "ruby "}));

        System.out.println("\n-------Task02------\n");
        System.out.println(removeNegatives(new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15))));

        System.out.println("\n-------Task03-----\n");
        String list = "Abcdefdsgnsdbgksjd";
        System.out.println(validatePassword(list));

        System.out.println("\n-------Task04-----\n");
        String email = "abcd@gmail.com";
        System.out.println(validateEmailAddress(email));


    }

    public static int countMultipleWords(String[] arr) {

        int count = 0;

        for (String s : arr) {
            if (!(s.isEmpty())) {
                if (s.trim().contains(" "))
                    count++;
            }
        }
        return count;
    }

    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> numbers) {

        numbers.removeIf(element -> element < 0);

        return numbers;
    }

    public static boolean validatePassword(String password) {
        if (password.length() < 8 || password.length() > 16) {
            return false;
        }
        boolean isDigit = false;
        boolean uppers = false;
        boolean lowers = false;
        boolean special = false;
        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);
            if (Character.isDigit(c)) {
                isDigit = true;
            }
                if (Character.isUpperCase(c)) {
                    uppers = true;
                }
                    if (Character.isLowerCase(c)) {
                        lowers = true;
                    }
                        if (!Character.isLetterOrDigit(c) && !Character.isWhitespace(c)){
                            special = true;
                        }
                    }
        if (!isDigit || !uppers || !lowers || !special) {
            return false;
        }
        if (password.contains(" ")) {
            return false;
        }
        return true;
    }
    public static boolean validateEmailAddress(String email){

            if (email.contains(" ")) {
                return false;
            }
            if (email.indexOf("@") < 2 || email.indexOf("@") != email.lastIndexOf("@")){
                return false;
        }
            int dot = email.lastIndexOf(".");
            if (dot < email.indexOf("@") + 2 || dot >= email.length() + 2 || dot <= email.indexOf("@")){
                return false;
            }
        return true;
    }
}

