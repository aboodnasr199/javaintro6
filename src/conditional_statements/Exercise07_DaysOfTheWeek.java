package conditional_statements;

import java.util.Random;

public class Exercise07_DaysOfTheWeek {
    public static void main(String[] args) {

        Random r1 = new Random();
       int number = r1.nextInt(7);


        switch (number){
            case 0:
                System.out.println("Sunday");
                break;
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");

        }
    }
}
