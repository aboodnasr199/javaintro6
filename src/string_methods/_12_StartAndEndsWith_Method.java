package string_methods;

import utilities.ScannerHelper;

public class _12_StartAndEndsWith_Method {
    public static void main(String[] args) {

        /*
        1. return
        2. return boolean
        3.
         */
        String str = "TechGlobal";

        boolean startsWith = str.startsWith("T");
        boolean endsWith = str.endsWith("T");

        System.out.println(startsWith); // true
        System.out.println(endsWith); // false

        System.out.println(str.startsWith("Techgl")); // false
        System.out.println(str.endsWith("TechGlobal")); // true



    }
}
