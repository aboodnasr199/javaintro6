package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ArrayPracticeWithYaya {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(duplicates(new int[]{5, 5, 2, 1, 4, 4, 3, 7})));

        System.out.println(countDup(new ArrayList<>(Arrays.asList(5, 5, 2, 1, 4, 4, 3, 7))));



    }
    public static  int[] duplicates(int[] arr){

        ArrayList<Integer> arrayList = new ArrayList<>();

        for (int j : arr) {
          if (!arrayList.contains(j)) arrayList.add(j);

        }
        int[] list = new int[arrayList.size()];

        for (int i = 0; i < arrayList.size(); i++) {
            list[i] = arrayList.get(i);

        }
        return list;
    }

    public static int countDup(ArrayList<Integer> list){
        HashSet<Integer> newList = new HashSet<>();
        int count = 0;

        for (int s : list) {
            if (!newList.add(s))
                count++;

        }
        return count;
    }
}
