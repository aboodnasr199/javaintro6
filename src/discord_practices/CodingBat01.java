package discord_practices;

public class CodingBat01 {
    public static void main(String[] args) {
        //Given an array of ints, return true if 6 appears as either the first or last element in the array.
        // The array will be length 1 or more.
        int[] arr = {6,1,2,3,4,6};
       firstLast6(arr);
    }

    public static boolean firstLast6(int[] nums) {
        if (nums[0] == 6 || nums[nums.length - 1] == 6) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        return false;
    }
}