package homeworks;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Homework11 {
    public static void main(String[] args) {
        System.out.println("\n--------Task1----------\n");
        System.out.println(noSpace("    Hello     "));
        System.out.println(noSpace(" Hello World  "));

        System.out.println("\n--------Task2----------\n");
        System.out.println(replaceFirstLast("A"));
        System.out.println(replaceFirstLast("Tech Global"));
        System.out.println(replaceFirstLast("Hello"));

        System.out.println("\n--------Task3----------\n");
        System.out.println(hasVowel(""));
        System.out.println(hasVowel("Java"));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel("ABC"));

        System.out.println("\n--------Task4---------\n");
        checkAge(2010);
        checkAge(2006);
        checkAge(2050);
        checkAge(1920);

        System.out.println("\n--------Task5---------\n");
        System.out.println(averageOfEdges(0, 0, 0));
        System.out.println(averageOfEdges(0, 0, 6));
        System.out.println(averageOfEdges(-2, -2, 10));

        System.out.println("\n--------Task6---------\n");
        System.out.println(Arrays.toString(noA(new String[]{"java", "hello", "123", "xyz"})));
        System.out.println(Arrays.toString(noA(new String[]{"appium", "123", "ABC", "java"})));

        System.out.println("\n--------Task7---------\n");
        System.out.println(Arrays.toString(no3or5(new int[]{7, 4, 11, 23, 17})));
        System.out.println(Arrays.toString(no3or5(new int[]{3, 4, 5, 6})));

        System.out.println("\n--------Task8---------\n");
        System.out.println(countPrimes(new int[]{-10, -3, 0, 1}));
        System.out.println(countPrimes(new int[]{7, 4, 11, 23, 17}));
        System.out.println(countPrimes(new int[]{41, 53, 19, 47, 67}));



    }

    public static String noSpace(String str) {

        return str.replaceAll(" ", "");
    }

    public static String replaceFirstLast(String str) {
        str = str.trim();
        int length = str.length();

        if (length < 2) {
            return "";
        }

        char first = str.charAt(0);
        char last = str.charAt(length - 1);
        String middle = str.substring(1, length - 1);
        return last + middle + first;
    }
    public static boolean hasVowel(String str){
        return str.toLowerCase().contains("a") || str.toLowerCase().contains("e")
                || str.toLowerCase().contains("o") || str.toLowerCase().contains("u")
                || str.toLowerCase().contains("i");
    }
    public static void checkAge(int yearOfBirth){

            Date currentDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentDate);
            int currentYear = calendar.get(Calendar.YEAR);


            int age = currentYear - yearOfBirth;

            if (age < 0 || age > 100) {
                System.out.println("AGE IS NOT VALID");
            }

            else if (age < 16) {
                System.out.println("AGE IS NOT ALLOWED");
            }

            else {
                System.out.println("AGE IS ALLOWED");
            }
        }
    public static int averageOfEdges(int a, int b, int c) {

        int min = Math.min(Math.min(a, b), c);
        int max = Math.max(Math.max(a, b), c);


        return (min + max) / 2;
    }
    public static String[] noA(String[] arr) {
        String[] newArr = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().startsWith("a")) {
                newArr[i] = "###";
            } else {
                newArr[i] = arr[i];
            }
        }
        return newArr;
    }
    public static int[] no3or5(int[] arr) {
        int[] newArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 3 == 0 && arr[i] % 5 == 0) {
                newArr[i] = 101;
            } else if (arr[i] % 5 == 0) {
                newArr[i] = 99;
            } else if (arr[i] % 3 == 0) {
                newArr[i] = 100;
            } else {
                newArr[i] = arr[i];
            }
        }
        return newArr;
    }
    public static int countPrimes(int[] arr) {
        int count = 0;
        for (int num : arr) {
            if (num > 1) {
                boolean isPrime = true;
                for (int i = 2; i <= Math.sqrt(num); i++) {
                    if (num % i == 0) {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime) {
                    count++;
                }
            }
        }
        return count;
    }

}






