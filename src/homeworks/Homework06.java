package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n------Task1------\n");
        int[] numbers = {89, 0, 23, 0, 12, 0, 15, 34, 0, 7, 0};

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n------Task2------\n");
        String[] str = {null, "abc", null, null, "xyz", null};

        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);
        System.out.println(Arrays.toString(str));

        System.out.println("\n------Task3------\n");

        int[] nums = {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(nums));

        Arrays.sort(nums);
        System.out.println(Arrays.toString(nums));

        System.out.println("\n------Task4------\n");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries));

        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        System.out.println("\n------Task5------\n");

        String[] cartoon = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoon));
        if (Arrays.toString(cartoon).contains("Pluto")) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        System.out.println("\n------Task6------\n");

        String[] cats = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(cats);
        System.out.println(Arrays.toString(cats));

        if (Arrays.toString(cats).contains("Garfield") && Arrays.toString(cats).contains("Felix")) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        System.out.println("\n------Task7------\n");

        double[] digits = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(digits));

        for (double digit : digits) {
            System.out.println(digit);

        }

        System.out.println("\n------Task8------\n");

        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        System.out.println(Arrays.toString(characters));

        int letters = 0;
        int uppers = 0;
        int lowers = 0;
        int Digits = 0;
        int specialChars = 0;
        for (char character : characters) {
            if (Character.isLetter(character)) {
                letters++;
            }
            if (Character.isUpperCase(character)) {
                uppers++;
            } else if (Character.isLowerCase(character)) {
                lowers++;
            } else if (Character.isDigit(character)) {
                Digits++;
            } else {
                specialChars++;
            }


        }

        System.out.println("Letters  = " + letters);
        System.out.println("Uppercase letters = " + uppers);
        System.out.println("Lowercase letters = " + lowers);
        System.out.println("Digits = " + Digits);
        System.out.println("Special Characters = " + specialChars);

        System.out.println("\n------Task9------\n");

        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        System.out.println(Arrays.toString(objects));

        int upperCase = 0;
        int lowerCase = 0;
        int BP = 0;
        int bookPen = 0;

        for (String object : objects) {
            if (Character.isUpperCase(object.charAt(0))) {
                upperCase++;
            } else if (Character.isLowerCase(object.charAt(0))) {
                lowerCase++;


            }
            if (object.toUpperCase().startsWith("B") || object.toUpperCase().startsWith("P")) {
                BP++;
            }
            if (object.toLowerCase().contains("book") || object.toLowerCase().contains("pen")) {
                bookPen++;
            }
        }
        System.out.println("Elements starts with Uppercase = " + upperCase);
        System.out.println("Elements starts with lowercase = " + lowerCase);
        System.out.println("Elements with B or P = " + BP);
        System.out.println("Elements having 'book' or 'pen = " + bookPen);

        System.out.println("\n------Task10------\n");

        int[] num2 = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(num2));
        int more = 0;
        int less = 0;
        int is = 0;
        for (int i : num2) {
            if (i > 10){
                more++;
            } else if (i < 10) {
                less++;
            } else {
                is++;

            }

        }
        System.out.println("Elements that are more than 10 = " + more);
        System.out.println("Elements that are less than 10 = " + less);
        System.out.println("Elements that are 10 = " + is);


        System.out.println("\n------Task11------\n");

        int[] array1 = {5, 8, 13, 1, 2};
        int[] array2 = {9, 3, 67, 1, 0};

        System.out.println("1st array is = " + Arrays.toString(array1));
        System.out.println("2nd array is = " + Arrays.toString(array2));

        int[] array3 = new int[array1.length];

        for (int i = 0; i < array1.length; i++) {
            array3[i] = Math.max(array1[i], array2[i]);

        }
        System.out.println("3rd array is = " + Arrays.toString(array3));
    }
}
