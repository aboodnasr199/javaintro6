package scanner_class;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Recap01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String firstName, address;
        int favNumber;

        System.out.println("what is your Name:");
        firstName = scanner.next();

        System.out.println("What is your address:");
        address = scanner.next();
        scanner.nextLine();


        System.out.println("What is your favorite number");
        favNumber = scanner.nextInt();

        System.out.println(firstName + "'s" + " address is " + address + " and their fav number is " + favNumber + ".");
    }
}
