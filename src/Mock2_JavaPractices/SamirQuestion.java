package Mock2_JavaPractices;

import java.util.ArrayList;
import java.util.Arrays;

public class SamirQuestion {
    public static void main(String[] args) {
        String[] list = {"Hello", "World", "Tech", "Global"};
        System.out.println((Arrays.toString(swapEnds(list))));

        String[] list1 = {"TechGlobal", "Batch6", "TechGlobal", "John Doe"};
        System.out.println(Arrays.toString(duplicates(list1)));

    }

    public static String[] swapEnds(String[] str){
        String temp = str[0];
        str[0] = str[str.length-1];
        str[str.length-1] = temp;

        return str;
    }
    public static String[] duplicates(String[] str){
    ArrayList<String> newList = new ArrayList<>();

        for (String s : str) {
            if (!newList.contains(s)) newList.add(s);
        }
        return newList.toArray(new String[0]);

    }
}
