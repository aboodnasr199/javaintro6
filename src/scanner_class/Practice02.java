package scanner_class;

import java.util.Scanner;

public class Practice02 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String fName, mName, lName, homeTown;

        System.out.println("First Name:");
        fName = scanner.next();

        System.out.println("Middle Name:");
        mName = scanner.next();
        scanner.nextLine();

        System.out.println("Last Name:");
        lName = scanner.next();
        scanner.nextLine();

        System.out.println("Full Name is: " + fName + " " + mName + " " + " " + lName);

        System.out.println("Hometown is: ");
        homeTown = scanner.nextLine();

        System.out.println("\nMy name is " + fName + " " + mName + " " + " " + lName + " and I am from \n" + homeTown + ".");



    }
}
