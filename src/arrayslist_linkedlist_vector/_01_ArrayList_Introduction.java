package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class _01_ArrayList_Introduction {
    public static void main(String[] args) {

        // 1. How to create an array vs arraylist
        System.out.println("\n----Task1-----\n");
        String[] array = new String[3]; // Array

        ArrayList<String> list = new ArrayList<>(); // capacity by default will be assigned = 10

        // 2. How to get size of an Array and Arraylist
        System.out.println("\n----Task2-----\n");
        System.out.println("The size of the array is = " + array.length); // array size in an Array
        System.out.println("The size of the array is = " + list.size());    // arraylist way.

        //3. How to print an array vs arraylist
        System.out.println("\n----Task3-----\n");
        System.out.println("The array = " + Arrays.toString(array)); // [null,null,null]
        System.out.println("The list = " + list); // []

        //4. how to add elements to an array vs arraylist
        System.out.println("\n----Task4-----\n");
        array[1] = "Alex"; // array
        array[2] = "Max"; // array
        array[0] = "Yahya"; // array
        System.out.println("The array = " + Arrays.toString(array));

        list.add("Joe"); // by default  adds to the tail of the list
        list.add("Jane");
        list.add("Mike");
        list.add("Adam");

        list.add(2, "Jazzy"); // you choose the location where the element can go.
        System.out.println("The list = " + list); //[Joe, Jane, Jazzy, Mike, Adam]

        //5. How to update an exisiting element in an array vs arraylist
        System.out.println("\n----Task5-----\n");
        array[1] = "Ali"; // updating it from "Alex" above. How to update using Arrays
        System.out.println("The array = " + Arrays.toString(array));

        list.set(1, "Jasmine"); // how to update an element using arrayslist: list.set method
        System.out.println("The list = " + list);

       //6. How to retrieve or get an element from array and arraylist
        System.out.println("\n----Task6-----\n");
        System.out.println(array[2]); // how to get a certain element
        System.out.println(list.get(3)); // how to get a certain element in array list

        //7 How to loop an array vs arraylist?
        System.out.println("\n----Task7 - fori loop-----\n");
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        } // How to loop an array

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));

        } // how to loop and print array lists

        System.out.println("\n----Task7 - for each loop-----\n");
        for (String element : array) {
            System.out.println(element);
        } // using for each loop with an array

        for (String element : list) {
            System.out.println(element);

        } // using for each loop to print out an array list (same thing as array)

        System.out.println("\n----forEach() method----\n");
        list.forEach(System.out::println); // lambda expression. this also prints out all the elements












    }
}
