package exceptions;

public class _02_Checked_CompileTime_Exceptions {
    public static void main(String[] args) {


        for (int i = 0; i < 10; i++) {
            System.out.println(i);
            try {
                Thread.sleep(1000);

            }catch (InterruptedException e){
                System.out.println("I caught an exception here!!");
            }

        }


        System.out.println("End of the program");
    }
}
