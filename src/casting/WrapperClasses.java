package casting;

public class WrapperClasses {
    public static void main(String[] args) {

        int age1 = 45; // takes 4 bytes

        Integer age2 = 45; // can take more than 4 bytes

        System.out.println(age1); // returns 45
        System.out.println(age2); // returns 45

        System.out.println(Integer.MAX_VALUE);
        System.out.println(Double.MIN_VALUE);
        System.out.println(Short.MIN_VALUE);

        String s = "25.5";

        System.out.println(Double.parseDouble(s) + 5); // returns 30.5
        System.out.println((int) Double.parseDouble(s) + 5); // returns 30

        String str = "12345";

        System.out.println(Integer.parseInt(str) + 5);

        char c1 = 'A';
        Character c2 = c1; // primitive into object -> auto boxing or boxing

        Character c3 = 'E';
        char c4 = c3; // object into primitive -> unboxing


    }
}
