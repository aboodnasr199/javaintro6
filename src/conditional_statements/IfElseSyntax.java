package conditional_statements;

public class IfElseSyntax {
    public static void main(String[] args) {

       // if else staements are used to control the flow of the program based on a condition
        // conditions should always be either true or false
        // if block can be used without an else block
        // else statemnts cannot be used without if


        boolean condition = true;

        if(condition){
           // this is if block will execute when condition is true
            System.out.println("A");
        }
        else {
            System.out.println("B");
        }
        System.out.println("End of the program");
    }
}
