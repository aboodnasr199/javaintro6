package arrays.practice05;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Mock4Test {
    public static void main(String[] args) {

        String[] arr = {"Apple", "Apple", "Orange", "Apple", "Kiwi"};

        System.out.println(uniqueElements(arr));


    }
    public static Map<String, Integer> uniqueElements(String[] arr){

        Map<String, Integer> uniques = new LinkedHashMap<>();

        for (String s : arr) {

            uniques.put(s, uniques.getOrDefault(s, 0) + 1);

        }
        return uniques;
    }
}
