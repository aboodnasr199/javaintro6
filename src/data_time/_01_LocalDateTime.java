package data_time;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class _01_LocalDateTime {
    public static void main(String[] args) {

        System.out.println("\n----------LocalDate-----------\n");
        LocalDate currentDate = LocalDate.now();

        System.out.println(currentDate); // 2023-05-02 yyyy-MM-dd

        System.out.println(currentDate.getYear());
        System.out.println(currentDate.getDayOfYear());
        System.out.println(currentDate.plusYears(2));
        System.out.println(currentDate.minusDays(5));
        System.out.println(currentDate.plusMonths(1));



        System.out.println("\n----------LocalTime-----------\n");
        LocalTime currentTime = LocalTime.now();

        System.out.println(currentTime); // 18:14:32.855 hh:mm:ss.SSS

        //hh:mm:ss a 18:35:45 PM

        System.out.println(currentTime.plusHours(6).format(DateTimeFormatter.ofPattern("hh:mm:ss a")));


        System.out.println("\n----------LocalDateTime-----------\n");
        LocalDateTime currentDateTime = LocalDateTime.now();

        System.out.println(currentDateTime);// 2023-05-02T18:16:11.107 yyyy-MM-ddThh:mm:ss.SSS

        System.out.println(currentDateTime.format(DateTimeFormatter.ofPattern("MMM d, yyyy h:mm a")));


        System.out.println(ChronoUnit.DAYS.between(LocalDate.of(2023, Month.JANUARY, 23), LocalDate.now()));
        System.out.println(ChronoUnit.DAYS.between(LocalDate.of(2000, Month.JANUARY, 1), LocalDate.now()));
        System.out.println(ChronoUnit.MONTHS.between(LocalDate.of(2000, Month.JULY, 25), LocalDate.now()));
        System.out.println(ChronoUnit.DAYS.between(LocalDate.of(2000, Month.JULY, 25), LocalDate.now()));

    }
}

