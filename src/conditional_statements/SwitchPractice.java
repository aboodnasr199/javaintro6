package conditional_statements;

import java.util.Scanner;

public class SwitchPractice {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter either a, b, or c");
        String letter = input.nextLine();

        switch (letter) {
            case "a":
                System.out.println("The letter you entered is a");
                break;


            case "b": {
                System.out.println("You entered letter b");
                break;
            }
            case "c": {
                System.out.println("You entered letter c");
                break;
            }
            default: {
                System.out.println("ERROR! THIS IS NOT A LETTER FROM a to c");
            }
        }
    }
}
