package concatenation;

public class Excercise01 {
    public static void main(String[] args) {
        System.out.println("\n----TASK 1-----\n");
        System.out.println("Hello World");
        System.out.println("Hello" + " World");
        System.out.println("Hello" + " " + "World");
        System.out.println("Today" + " " + "is" + " " + "Sunday");

        System.out.println("\n----TASK 2-----\n");
        System.out.println("123" + " " + "12"); //

        /*
                12
             ab
         */
        System.out.println("\t" + "12" + "\t\nab");
    }
}
