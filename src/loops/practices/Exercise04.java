package loops.practices;

import utilities.ScannerHelper;

public class Exercise04 {
    public static void main(String[] args) {
     String str = ScannerHelper.getString();
     int vowels = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = Character.toLowerCase(str.charAt(i));
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') vowels++;
        }
        System.out.println("This string has " + vowels + " vowels");

    }
}
