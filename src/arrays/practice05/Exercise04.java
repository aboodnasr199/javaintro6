package arrays.practice05;

public class Exercise04 {
    public static void main(String[] args) {
        String[] list = {"banana", "orange", "Apple"};
        list(list);
    }
    public static void list(String[] list){
        boolean has = false;
        for (String s : list) {
            if (s.equalsIgnoreCase("apple")) {
                has = true;
                break;
            }
        }
        System.out.println(has);
    }
}
