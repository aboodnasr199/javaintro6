package math_class;

public class FindMax {
    public static void main(String[] args) {
        // Finding the max of two numbers
        int  num1 = 10;
        int num2 = 15;

        int max = Math.max(num1, num2);

        System.out.println(max);

        // finding the max of 4 numbers

        int num3 = 2, num4 = 8, num5 = 5, num6 = 18;

        int max1 = Math.max(num3, num4);
        int max2 = Math.max(num5, num6);


        System.out.println(Math.max(max1, max2));

        //FINDING max of 3 numbers

        int number1 = -30;
        int number2 = -40;
        int number3 = 0;

        int max3 = Math.max(number1, number2);

        System.out.println(Math.max(max3, number3));

        //FINDING MAX OF 5 numbers

        int a = 5;
        int b = 10;
        int c = 50;
        int d = 189;
        int f = 12;

        System.out.println(Math.max(Math.max(Math.max(a,b), Math.max(c,d)), f));

        System.out.println(Math.max(5,5)); // returns 5

        Math.max(10 + 25, 45-10); // returns 35

        System.out.println(Math.min(Math.max(5 - 25, -32), Math.min(10, 9))); // returns -20


    }
}
