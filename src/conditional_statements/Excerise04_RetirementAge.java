package conditional_statements;

import java.util.Scanner;

public class Excerise04_RetirementAge {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int age1;

        System.out.println("Please enter your age?");
        age1 = input.nextInt();


        if (age1 >= 55){
            System.out.println("It is time to get retired!");
        }
        else if (age1 < 55){
            int left = 55 - age1;
            if (left == 1){
                System.out.println("You have 1 year to be retired.");
            }
            else
            System.out.println("You have " + left + " years to be retired ");
        }

    }
}
