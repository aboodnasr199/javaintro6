package conditional_statements;

import java.util.Scanner;

public class Exercise08_Switch {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String dayOfTheWeek;
        System.out.println("Please enter the day of the week");
        dayOfTheWeek = input.next();

        switch (dayOfTheWeek){
            case "monday":
                System.out.println("1st day of the week");
                break;

            case "tuesday":
                System.out.println("2nt day of the week");
                break;

            case "wednesday":
                System.out.println("3rd day of the week");
                break;

            case "thursday":
                System.out.println("4th day of the week");
                break;

            case "friday":
                System.out.println("5th day of the week");
                break;

            case "saturday":
                System.out.println("6th day of the week");
                break;

            case "sunday":
                System.out.println("7th day of the week");
                break;

            default:
                System.out.println("ANYTHING ELSE");
        }

    }
}
