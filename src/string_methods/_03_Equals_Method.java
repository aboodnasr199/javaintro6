package string_methods;

public class _03_Equals_Method {
    public static void main(String[] args) {

        String str1 = "Tech";
        String str2 = "Global";
        String str3 = "tech";
        String str4 = "TechGlobal";


        boolean isEquals = str1.equals(str2);
        System.out.println(isEquals);

        System.out.println(str1.equals(str3));

        System.out.println(str1.concat(str2).equals(str4));


    }
}
