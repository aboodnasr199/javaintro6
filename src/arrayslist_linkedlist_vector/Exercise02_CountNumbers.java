package arrayslist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;

public class Exercise02_CountNumbers {
    public static void main(String[] args) {
        System.out.println(countEven(new ArrayList<>(Arrays.asList(2, 3, 5))));
        System.out.println(countEven(new ArrayList<>(Arrays.asList(10, 20, 30))));
        System.out.println(countEven(new ArrayList<>(Arrays.asList())));
        System.out.println(countEven(new ArrayList<>(Arrays.asList(-1, 3, 17, 25))));

        more15(new ArrayList<>(Arrays.asList(10,5,2, -20)));
        more15(new ArrayList<>(Arrays.asList(100,200, 45)));
        more15(new ArrayList<>(Arrays.asList()));

        System.out.println(no3(new ArrayList<>(Arrays.asList(13, 3, 30, 300, 533))));
        System.out.println(no3(new ArrayList<>(Arrays.asList(0,1,30, 13))));
        System.out.println(no3(new ArrayList<>(Arrays.asList())));
        System.out.println(no3(new ArrayList<>(Arrays.asList(3333))));





    }

    public static int countEven(ArrayList<Integer> numbers) {
     return (int) numbers.stream().filter(element -> element % 2 == 0).count();
    }

    public static void more15(ArrayList<Integer> list){
        System.out.println(list.stream().filter(element -> element > 15).count());
    }

    public static int no3(ArrayList<Integer> nums){
      return (int) nums.stream().filter(x -> x.toString().contains("3")).count();
      /*
        int count = 0;
         for (Integer num : nums) {
            if (num.toString().contains("3")) count++;
        }
        return count;
       */
    }
}
