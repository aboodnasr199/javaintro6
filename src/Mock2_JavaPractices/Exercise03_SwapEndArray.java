package Mock2_JavaPractices;

import java.util.Arrays;

public class Exercise03_SwapEndArray {
    public static void main(String[] args) {
        // reversing an array
        int[] arr1 = {0,1,2,3,4,5,6};
        int[] arrRev = new int[arr1.length];

        int j = 0;
        for (int i = arr1.length-1; i >= 0 ; i--) {
            arrRev[i] += arr1[j];
            j++;
        }
        System.out.println(Arrays.toString(arrRev));


        String str = "Java is fun alot";

        System.out.println(str.split(" ").length);


        //Swapping first and last index
        int[] arr = {0,1,2,3,4,5,6};
        int firstNumber = arr[0];
        arr[0] = arr[arr.length-1];
        arr[arr.length-1] = firstNumber;

        System.out.println(Arrays.toString(arr));
    }
}
