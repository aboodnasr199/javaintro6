package operators.arithmetic_operators;

public class DivisionPractices {
    public static void main(String[] args) {
        double i1 = 20, i2 = 8;

        double divison = (i1 / i2);

        System.out.println("The divison of " + i1 + " by " + i2 + " = " + divison);
    }
}
