package homeworks;

import java.util.Random;
import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.println("\n-------task1------\n");

        System.out.println("Please enter 2 numbers");
        int n1 = input.nextInt();
        int n2 = input.nextInt();
        int abs = Math.abs(n1) + Math.abs(n2);

        System.out.println("The difference between numbers is = " + abs);

        System.out.println("\n-------task2------\n");

        System.out.println("Please enter 5 numbers");
        int x1 = input.nextInt();
        int x2 = input.nextInt();
        int x3 = input.nextInt();
        int x4 = input.nextInt();
        int x5 = input.nextInt();

        int maxValue = Math.max(Math.max(Math.max(x1, x2), (Math.max(x3, x4))), x5);
        int minValue = Math.min(Math.min(Math.min(x1, x2), (Math.min(x3, x4))), x5);

        System.out.println("Max Value = " + maxValue);
        System.out.println("Min Value = " + minValue);



        System.out.println("\n-------task3------\n");

        Random number = new Random();
        int number1 = number.nextInt(51) + 49;
        int number2 = number.nextInt(51) + 49;
        int number3 = number.nextInt(51) + 49;

        System.out.println(number1);
        System.out.println(number2);
        System.out.println(number3);
        System.out.println("The sum of the numbers is = " + (number1 + number2 + number3));



        System.out.println("\n-------task4------\n");

        double alex = 125, mike = 220;

        System.out.println("Alex's money = " + (alex - 25.5));
        System.out.println("Mike's money = " + (mike + 25.5));



        System.out.println("\n-------task5------\n");

        int total = 390;
        double dailySave = 15.60;

        System.out.println(( total / dailySave));



        System.out.println("\n-------task6------\n");

        String s1 = "5", s2 = "10";

        System.out.println("Sum of 5 and 10 is = " + (Integer.parseInt(s1) + Integer.parseInt(s2)));
        System.out.println("Product of 5 and 10 is = " + (Integer.parseInt(s1) * Integer.parseInt(s2)));
        System.out.println("Division of 5 and 10 is = " + (Integer.parseInt(s1) / Integer.parseInt(s2)));
        System.out.println("Subtraction of 5 and 10 is = " + (Integer.parseInt(s1) - Integer.parseInt(s2)));
        System.out.println("Remainder of 5 and 10 is = " + + (Integer.parseInt(s1) % Integer.parseInt(s2)));




        System.out.println("\n-------task7------\n");

        String z1 = "200", z2 = "-50";

        System.out.println("The greatest value is = " + Math.max(Integer.parseInt(z1), Integer.parseInt(z2)));
        System.out.println("The smallest value is = " + Math.min(Integer.parseInt(z1), Integer.parseInt(z2)));
        System.out.println("The average is = " + (Integer.parseInt(z1) + Integer.parseInt(z2)) / 2);
        System.out.println("The absolute difference is = " + Math.abs(Integer.parseInt(z1) +
                Math.abs(Integer.parseInt(z2))));



        System.out.println("\n-------task8------\n");

        double quarterDaily = .75, dimeDaily = .10, nickleDaily = .10, pennyDaily = .01;

        System.out.println((Math.round (24 / (quarterDaily + dimeDaily + nickleDaily + pennyDaily))) + " days.");
        System.out.println((Math.round(168 / (quarterDaily + dimeDaily + nickleDaily + pennyDaily))) + " days.");
        System.out.println( "$" + (150 *(quarterDaily + dimeDaily + nickleDaily + pennyDaily)));

        System.out.println("\n-------task9------\n");

        double saving = 1250, dailySaving = 62.5;

        System.out.println(Math.round(saving / dailySave));



        System.out.println("\n-------task10------\n");

        double save = 14265, paymentOption1 = 475.50, paymentOption2 = 951;

        System.out.println("Option 1 will take " + Math.round(save / paymentOption1));
        System.out.println("Option 2 will take " + Math.round(save / paymentOption2));



        System.out.println("\n-------task11------\n");

        System.out.println("Please enter 2 numbers");
        int num1 = input.nextInt();
        int num2 = input.nextInt();

        System.out.println((double) num1 / num2);


        System.out.println("\n-------task12------\n");

        Random random = new Random();
        int y1 = random.nextInt(101);

        if (y1 > 25){
            System.out.println(true);
        }
        else if (y1 <= 25){
            System.out.println(false);
        }



        System.out.println("\n-------task13------\n");

        Random random1 = new Random();
        int u1 = random1.nextInt(8);

        switch (u1){
            case 0:
                System.out.println("Monday");
                break;
            case 1:
                System.out.println("Tuesday");
                break;
            case 2:
                System.out.println("Wednesday");
                break;
            case 3:
                System.out.println("Thursday");
                break;
            case 4:
                System.out.println("Friday");
                break;
            case 5:
                System.out.println("Saturday");
                break;
            case 6:
                System.out.println("Sunday");
        }

        System.out.println("\n-------task14------\n");

        System.out.println("Please enter your exam resutls?");
        int exam1 = input.nextInt();
        int exam2 = input.nextInt();
        int exam3 = input.nextInt();

        int average = (exam1 + exam2 + exam3) / 3;

        if (average > 70) {
            System.out.println("YOU PASSED!");
        }
        else {
            System.out.println("YOU FAILED!");
        }


        System.out.println("\n-------task15------\n");

        System.out.println("Please enter 3 numbers");
        int digit1 = input.nextInt();
        int digit2 = input.nextInt();
        int digit3 = input.nextInt();

     if (digit1 == digit2 && digit2 == digit3){
         System.out.println("TRIPLE MATCH");
     }
    else if (digit1 == digit2 || digit1 == digit3){
         System.out.println("DOUBLE MATCH");
     }
    else if (digit1 != digit2 && digit2 != digit3){
         System.out.println("NO MATCH");
     }

    }


    }
