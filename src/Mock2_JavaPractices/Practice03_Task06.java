package Mock2_JavaPractices;

import utilities.ScannerHelper;

public class Practice03_Task06 {
    public static void main(String[] args) {
        String s = ScannerHelper.getString();
        startEnd(s);
    }
    public static void startEnd(String str){
        if (str.length() < 4){
            System.out.println("Invalid Input");
        }
        else System.out.println(str.startsWith("xx") && str.endsWith("xx"));
    }
}
