package print_statements;

public class PrintVsPrintln {

    public static void main (String[] args) {
        //I will write code statements here
        //println method
        System.out.println("Hello World");
        System.out.println("Today is sunday");
        System.out.println("john doe");



        //print method
        System.out.print("Hello World");
        System.out.print("Today is sunday");
        System.out.print("john doe");
    }
}
