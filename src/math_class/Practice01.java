package math_class;

import java.util.Scanner;

public class Practice01 {
    public static void main(String[] args) {

        System.out.println("\n------Task1-----\n");

        int randomNumber = (int)(Math.random() * 51);
        int result = randomNumber * 5;
        System.out.println(randomNumber);
        System.out.println("The random number * 5 = " + result);

        System.out.println("\n------Task2-----\n");
/*
        int randomNumber1 = (int)(Math.random() * 10);

        int max = Math.max(randomNumber1, randomNumber1);
        int min = Math.min(randomNumber1, randomNumber1);
        int difference = Math.abs(randomNumber1);

        System.out.println("Min number = " + min);
        System.out.println("Max number = " + max);
        System.out.println("difference = " + difference);
        THIS IS HOW I DID IT
 */
       // THE CORRECT WAY
        /*
        int random1 = (int) (Math.random() * 10 + 1);
        int random2 = (int) (Math.random() * 10 + 1);

        System.out.println("Min number = " + Math.min(random1, random2));
        System.out.println("Max number = " + Math.max(random1, random2));
        System.out.println("difference = " + Math.abs(random1-random2));

         */
        System.out.println("\n------Task3-----\n");

        int random = (int) (Math.random() * 51 + 50);

        int remainder = random % 10;

        System.out.println("The random number % 10 = " + remainder);

        System.out.println("\n------Task4-----\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 5 numbers bw 1-10");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        int num3 = input.nextInt();
        int num4 = input.nextInt();
        int num5 = input.nextInt();


        System.out.println(num1 * 5 + num2 * 4 + num3 * 3 + num4 * 2 + num5);


    }
}
