package exceptions;

import utilities.ScannerHelper;

import java.util.Scanner;

public class _01_Unchecked_Runtime_Exception {
    public static void main(String[] args) {

        String name = "John";

        int[] numbers = {10, 15, 20};

        String address = null;

        String name1 = ScannerHelper.getFirstName();

        try {
            System.out.println(name1.charAt(5));
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("I could not print char at 5 as it does not exist");
        }finally {
            System.out.println("I am here to run all the time");
        }

        System.out.println(name.charAt(2)); // h
        //System.out.println(name.charAt(-7)); // String index out of bounds

        System.out.println(address.toUpperCase()); // null pointer exception


        //System.out.println(numbers[5]); // ArrayIndexOutOfBounds




        System.out.println("End of the program");
    }
}
