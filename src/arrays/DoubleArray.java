package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {
        // create an array to story -> 5.5, 6, 10.3, 25

        double[] balances = {5.5, 6, 10.3, 25};

        // print the array

        System.out.println(Arrays.toString(balances));

        // print the size of the array
        System.out.println("The length is " + balances.length);

        // print elements using for each loop

        for (double balance : balances) {
            System.out.println(balance);

        }

        }
    }

