package string_methods;

public class _15_IsEmpty_Method {
    public static void main(String[] args) {

        /*
        1. return type
        2. returns boolean
        3. non static
        4. no arguments
         */
        String str = "";
        String word = "Hello";

        System.out.println("First String is empty = " + str.isEmpty()); // true
        System.out.println("First String is empty = " + word.isEmpty()); // false

    }
}
