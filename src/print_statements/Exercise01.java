package print_statements;

public class Exercise01 {
    public static void main(String[] args){
        //TASK 1
        System.out.println("Java is platform independent");
        System.out.println("Java is an OOP language");

        //Task 2
        System.out.print("j");
        System.out.print("a");
        System.out.print("v");
        System.out.print("a");

    }
}
