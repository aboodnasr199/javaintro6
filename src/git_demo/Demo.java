package git_demo;

public class Demo {
    public static void main(String[] args) {
        String firstName = "Adam";
        String lastName = "Nasr";

        System.out.println(firstName + lastName);

        String favColor = "Black";
        System.out.println("My favorite color is " + favColor);

        String favCar = "Porsche";
        int year = 2023;
        System.out.println("My favorite car is " + favCar + "and the model year is " + year);
    }
}
