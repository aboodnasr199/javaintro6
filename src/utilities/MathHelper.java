package utilities;

public class MathHelper {

    public static int findMax(int n1, int n2, int n3){
      return Math.max(Math.max(n1, n2), n3);
    }
    public static int sum (int num1, int num2){
    return (num1 + num2);

    }
    public static int sum (int a, int b, int c){
        return a + b + c;
    }
    public static int product(int number1, int number2){
        return (number1 * number2);
    }
    public static int square(int a){
        return (int) Math.sqrt(a);
    }
}
