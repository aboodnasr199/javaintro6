package Mock2_JavaPractices;

import utilities.ScannerHelper;

public class Exercise01_Palindrome {
    public static void main(String[] args) {

        //Palindrome
        String word = "civic";
        String reversedWord = "";

        for (int i = word.length()-1; i >= 0 ; i--) {
            reversedWord += word.charAt(i);

        }
        System.out.println(word.equals(reversedWord));


    }
}
