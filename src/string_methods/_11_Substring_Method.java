package string_methods;

public class _11_Substring_Method {
    public static void main(String[] args) {

       // TechGlobal? Substring = Tech or Global or ch or echGlo

        String str = "I love java a lot";
        // beg index to ending index
        String firstWord = str.substring(0, 1);  // I
        String secondWord = str.substring(2, 6); // love
        String thirdWord = str.substring(7, 11); // java

        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);
        // beg index only
        System.out.println(str.substring(0)); // java
        System.out.println(str.substring(str.length() / 2));
    }
}
