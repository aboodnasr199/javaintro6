package loops.practices;

public class Exercise05 {
    public static void main(String[] args) {

        String a = "";
        int num1 = 0;
        int num2 = 1;
        int c = 1;
        while (c <= 7){
            a += num1 + " - ";
            int sum = num1 + num2;
            num1 = num2;
            num2 = sum;
            c++;
        }
        System.out.println(a.substring(0,a.length()-3));

        System.out.println("BILALS WAY");

        int fib = 7;
        int numb1 = 0, numb2 = 1, n;
        String answer = "";
        for (int i = 1; i <= fib ; i++) {
            answer += numb1 + " - ";
            n = numb1 + numb2;
            numb1 = numb2;
            numb2 = n;

        }
        System.out.println(answer.substring(0, answer.length()-3));
    }
}
