package collections;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class Exercise03_CalculateCharacters {
    public static void main(String[] args) {


        System.out.println("\n-------Task1--------\n");

        String str = "banana";

        HashMap<Character, Integer> letters = new HashMap<>();

        for (char letter : str.toCharArray()) {
            if (!letters.containsKey(letter)){
                letters.put(letter, 1);
            }else{
                letters.put(letter, letters.get(letter) + 1);
            }
        }
        System.out.println(letters);


        for (Map.Entry<Character, Integer> entry: letters.entrySet()) {
            if (entry.getValue() > 1) System.out.println(entry.getKey());
        }

        System.out.println("\n------------Task2----------\n");

        String str1 = "Pineapple";

        Map<Character, Integer> characters = new HashMap<>();

        for (char letter : str1.toCharArray()) {
            if(characters.containsKey(letter)){
                characters.put(letter, characters.get(letter) + 1);
            }else {
                characters.put(letter, 1);
            }
        }
        System.out.println(characters);

        int max =  new TreeSet<>(characters.values()).last();

        System.out.println(max);


        for (Map.Entry<Character, Integer> entry: characters.entrySet()){
            if (entry.getValue() == max) System.out.println("Character \"" + entry.getKey() + "\" is the most counted with " + entry.getValue() + " times.");


        }


    }
}
