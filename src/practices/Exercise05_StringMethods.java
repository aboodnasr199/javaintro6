package practices;

import utilities.ScannerHelper;

public class Exercise05_StringMethods {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();
        String str1 = ScannerHelper.getString();

        System.out.println(str.length() < 2 || str1.length() < 2 ? "INVALID INPUT"
                : str.substring(1, str.length() - 1) + str1.substring(1, str1.length() - 1));
    }
}
