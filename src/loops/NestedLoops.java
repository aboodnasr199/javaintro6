package loops;

public class NestedLoops {
    public static void main(String[] args) {
        for (int i = 1; i <= 3 ; i++) {
            System.out.println("outer loop value = " + i);

            // inner loop
            for (int j = 1; j <= 5 ; j++) {
                System.out.println("\tInner loop value = " + j);

            }
                }
                for (int j = 1; j <= 12 ; j++) {
                    System.out.println("This is month = " + j);

                    for (int k = 1; k <=30 ; k++)
                        System.out.println("\tThis is day = " + k);

        }
    }
}
