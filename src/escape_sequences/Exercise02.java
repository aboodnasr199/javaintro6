package escape_sequences;

public class Exercise02 {
    public static void main(String[] args) {
        /*
                Steve Ballmer replaced Gates as CEO in 2000, and later envisioned a "devices and services" Strategy.
         */
        System.out.println("\n----TASK 1-----\n");
        System.out.println("Steve Ballmer replaced as CEO in 2000, and later envisioned a \"devices and services\" Strategy");

        //TASK 1
        System.out.println("\n----TASK 2-----\n");
        System.out.println("I like \"Sunday\" and apple");

        System.out.println("\n----TASK 3-----\n");
        System.out.println("My fav fruits are \"Kiwi\" and \"Orange\"");


    }
}
