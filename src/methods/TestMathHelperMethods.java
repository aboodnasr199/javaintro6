package methods;

import utilities.MathHelper;
import utilities.Printer;

import java.util.Random;

public class TestMathHelperMethods {
    public static void main(String[] args) {

        Printer.printGM();
        Printer.printGM();


        Printer.helloName("Jazzy");
        Printer.helloName("Mathew");
        Printer.helloName("Adam");


        // Static vs non static method
        // Static can be invoked with class names
        // non static can be invoked with an object of the class

        Printer myPrinter = new Printer();
        myPrinter.printTechGlobal();

        Random r =new Random();
        r.nextInt();

        int i = MathHelper.sum(3, 5); // returns 8

        Printer.printGM(); // it does not return anything

        System.out.println(MathHelper.sum(3, 5)); // 8

        // System.out.println(Printer.printGM()); // compiler error




    }
}
