package math_class;

public class FindAbs {
    public static void main(String[] args) {

        // -35 -> |-35|

        int num = -100;

        System.out.println(Math.abs(num)); // converts negative number to positive
        System.out.println(Math.abs(50-55));
        System.out.println(Math.abs(5));
        System.out.println(Math.abs(-5.68271287));
    }
}
