package arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Exercise02_CountStrings {
    public static void main(String[] args) {

        // 1. declare a string called countries and assign size of 3
        String[] countries = new String[3];

        // 2. assign "Spain" to index of 1
        countries[1] = "Spain";

        //3. print the values at index of 1 and 2
        System.out.println(countries[1]);
        System.out.println(countries[2]);

        //4. assign "Belgium" at index of 0 and "Italy" at index of 2
        countries[0] = "Belgium";
        countries[2] = "Italy";

        // 5.
        System.out.println(Arrays.toString(countries));

        // 6. sort this array and print it again
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        //7. print each element with fori loop
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);

        }
        //8. print each element with for each loop
        for (String country : countries) {
            System.out.println(country);

        }

        //9. count how many countries has 5 characters in names -> 2
        int count = 0;
        for (String country : countries) {
            if (country.length()  == 5) count++;


        }
        System.out.println(count);

        // 10. count how many countries has letter I or i in their name -> 3
        int i = 0;
        for (String country : countries) {
            if (country.toLowerCase().contains("i")) i++;

        }
    }
}
