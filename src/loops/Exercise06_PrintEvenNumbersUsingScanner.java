package loops;

import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumbersUsingScanner {
    public static void main(String[] args) {
        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        for (int j = Math.min(num1, num2); j <= Math.max(num1, num2); j++)
            if (j % 2 ==0) System.out.println(j);

    }
}
