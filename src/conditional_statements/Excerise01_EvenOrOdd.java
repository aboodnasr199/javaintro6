package conditional_statements;

import java.util.Scanner;

public class Excerise01_EvenOrOdd {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number?");
        int num1 = input.nextInt();

       if (num1 % 2 == 0) {
            System.out.println("The number you entered is even");
        }
       else {
           System.out.println("The number you entered is odd");
        }

        System.out.println("End of Program");
    }
}
