package exceptions.custom_exceptions;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println(checkAge(16));
       // System.out.println(checkAge(120));
        //System.out.println(checkAge(121));
       // System.out.println(checkAge(-1));

        System.out.println(isCheckInHours(12));

    }

    public static boolean checkAge(int age){
        if (age >= 16 && age <= 120) return true;
        else if (age > 0 && age < 16) return false;
        else throw new RuntimeException();
    }

    public static boolean isCheckInHours(int day){
        if (day >= 1 && day <= 7) return true;
        else throw new RuntimeException("The input does not represent any day!!!");
    }
}
