package arrays;

import java.util.Arrays;

public class SortingArrays {
    public static void main(String[] args) {

        int[] numbers = {5, 3, 10};
        String[] words = {"Alex", "ali", "John", "James"};

        // sort them - after this line you cannot get back to original array
        /*
        1. static: if you are able to invoke the method with class name -> className.methodName()
        2. void:
         */
        Arrays.sort(numbers);
        Arrays.sort(words);


        //print the arrays back
        System.out.println(Arrays.toString(numbers));
        System.out.println(Arrays.toString(words));
    }
}
