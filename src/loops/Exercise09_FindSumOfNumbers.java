package loops;

public class Exercise09_FindSumOfNumbers {
    public static void main(String[] args) {
        // find the sum of the given numbers 10-15 both included
       // 10 + 11 + 12 + 13 + 14 + 15 = 75
        // expected = 75
        int sum = 0;
        for (int i = 10; i <= 15; i++) {
            System.out.println(sum += i);

        }
    }
}
