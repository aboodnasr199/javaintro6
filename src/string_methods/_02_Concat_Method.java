package string_methods;

public class _02_Concat_Method {
    public static void main(String[] args) {

        String str1 = "Tech";
        String str2 = "Global";

        System.out.println(str1.concat(str2));
    }
}
